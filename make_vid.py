import argparse
import cv2
import os

# python make_vid.py --img_path=datasets/ForceEstimationRender/translated/cut/a/13 --video_path=test.avi

parser = argparse.ArgumentParser()
parser.add_argument('--img_path')
parser.add_argument('--video_path')

def main():
    args = parser.parse_args()
    # print(args.img_path, args.video_path)
    images = [img for img in os.listdir(args.img_path) if img.endswith(".png")]
    frame = cv2.imread(os.path.join(args.img_path, images[0]))
    height, width, layers = frame.shape

    # video = cv2.VideoWriter(args.video_path, 0, 10, (width,height))
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(args.video_path, fourcc, 10, (width, height))

    for image in images:
        video.write(cv2.imread(os.path.join(args.img_path, image)))

    # cv2.destroyAllWindows()
    video.release()

if __name__ == '__main__':
    main()