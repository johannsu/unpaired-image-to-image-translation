# Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks

Code for bachelor thesis

## Setup environment

create conda environment

```bash
conda create -n "<project-name>" python=3.11
```

activate conda environment

```bash
conda activate <project-name>
```

install requirements

```bash
pip install -r requirements.txt
```

(optional) install [blender 4.1.0](https://www.blender.org/download/)

### Activate Blender Node extension

1. change the path to your site-packages folder in the `node_pos.py` file in src/utils
2. open blender settings and go to the addons tab
3. click on install and select the `node_pos.py` file from src/utils
4. activate the addon

## Running the code

Type `python main.py -h` to see the available commands

### Training

To train the model run the following command (replace the config file with the desired one)

```bash
python main.py fit --config=configs/cycle_gan.yml
```

### Testing

To test a trained model run the following command (replace the config file with the desired one)

```bash
python main.py test --config=configs/cycle_gan.yml --ckpt_path checkpoints/<path-to-checkpoint-file>
```

### Inference

```bash
python main.py predict --config=configs/cycle_gan.yml --ckpt_path checkpoints/<path-to-checkpoint-file> --data.predict_from_path datasets/<path-to-dataset> --data.predict_to_path datasets/<path-to-dataset> --data.predict_recursive true
```

### Running on a slurm cluster

To run the experiments on a slurm cluster, use the following command.
For dependencies, use `-d=afterok:20:21,afterany:23`.

```bash
sbatch slurm/seg_real.sh
```

### Start tensorboard

To start tensorboard run (--bind_all if on remote server)

```bash
tensorboard --logdir=runs --bind_all --samples_per_plugin images=10000
```
