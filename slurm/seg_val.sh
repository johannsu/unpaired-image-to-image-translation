#!/bin/bash -l

# SLURM SUBMIT SCRIPT
#SBATCH -e err-Test-newB-0-%J.err
#SBATCH -o out-Test-newB-0-%J.out
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --nodes=1             # This needs to match Trainer(num_nodes=...)
#SBATCH --gpus=1
#SBATCH --ntasks-per-node=1   # This needs to match Trainer(devices=...)
#SBATCH --mem=16G
#SBATCH --signal=SIGUSR1@90

# activate conda env
source /home/suessjoha/.bashrc
conda activate env

# debugging flags (optional)
export NCCL_DEBUG=INFO
export PYTHONFAULTHANDLER=1

# on your cluster you might need these:
# set the network interface
# export NCCL_SOCKET_IFNAME=^docker0,lo

# might need the latest CUDA
# module load NCCL/2.4.7-1-cuda.10.0

# base model on real data
srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_0/checkpoint_epoch=084_f1_score=0.87.ckpt

# untranslated train:real->test:syn
srun python main.py test \
    --config configs/segmentation.yml \
    --data.render_mode raw \
    --data.include_renders_test true \
    --data.rd_id 1 \
    --data.real_render_ratio 0 \
    --ckpt_path checkpoints/segmentation/version_0/checkpoint_epoch=084_f1_score=0.87.ckpt

# untranslated train:real+syn->test:real
srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_1/checkpoint_epoch=068_f1_score=0.87.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_2/checkpoint_epoch=079_f1_score=0.85.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_3/checkpoint_epoch=026_f1_score=0.84.ckpt

# cyclegan train:real->test:syn
srun python main.py test \
    --config configs/segmentation.yml \
    --data.render_mode cycle_gan \
    --data.model_version version_0 \
    --data.include_renders_test true \
    --data.rd_id 1 \
    --data.real_render_ratio 0 \
    --ckpt_path checkpoints/segmentation/version_0/checkpoint_epoch=084_f1_score=0.87.ckpt

# cycle_gan train:real+syn->test:real
srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_4/checkpoint_epoch=025_f1_score=0.86.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_5/checkpoint_epoch=061_f1_score=0.86.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_6/checkpoint_epoch=047_f1_score=0.85.ckpt

# munit train:real->test:syn
srun python main.py test \
    --config configs/segmentation.yml \
    --data.render_mode munit \
    --data.model_version version_0 \
    --data.include_renders_test true \
    --data.rd_id 1 \
    --data.real_render_ratio 0 \
    --ckpt_path checkpoints/segmentation/version_0/checkpoint_epoch=084_f1_score=0.87.ckpt

# munit train:real+syn->test:real
srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_7/checkpoint_epoch=024_f1_score=0.86.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_8/checkpoint_epoch=018_f1_score=0.84.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_9/checkpoint_epoch=050_f1_score=0.81.ckpt

# cut train:real->test:syn
srun python main.py test \
    --config configs/segmentation.yml \
    --data.render_mode cut \
    --data.model_version version_0 \
    --data.include_renders_test true \
    --data.rd_id 1 \
    --data.real_render_ratio 0 \
    --ckpt_path checkpoints/segmentation/version_0/checkpoint_epoch=084_f1_score=0.87.ckpt

# cut train:real+syn->test:real
srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_10/checkpoint_epoch=047_f1_score=0.86.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_11/checkpoint_epoch=035_f1_score=0.84.ckpt

srun python main.py test \
    --config configs/segmentation.yml \
    --ckpt_path checkpoints/segmentation/version_12/checkpoint_epoch=026_f1_score=0.82.ckpt
