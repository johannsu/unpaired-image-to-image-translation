#!/bin/bash -l

# SLURM SUBMIT SCRIPT
#SBATCH -e err-Test-newB-0-%J.err
#SBATCH -o out-Test-newB-0-%J.out
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --nodes=1             # This needs to match Trainer(num_nodes=...)
#SBATCH --gpus=1
#SBATCH --ntasks-per-node=1   # This needs to match Trainer(devices=...)
#SBATCH --mem=16G
#SBATCH --signal=SIGUSR1@90
#SBATCH --array=1,2,3,5,7,10,20

# activate conda env
source /home/suessjoha/.bashrc
conda activate env

# debugging flags (optional)
export NCCL_DEBUG=INFO
export PYTHONFAULTHANDLER=1

# on your cluster you might need these:
# set the network interface
# export NCCL_SOCKET_IFNAME=^docker0,lo

# might need the latest CUDA
# module load NCCL/2.4.7-1-cuda.10.0

# hyperparameter search for cycle gan
## ms_ssim weight
srun python main.py fit \
    --config=configs/cycle_gan.yml \
    --model.cycle_w 1.0 \
    --model.identity_w 10.0 \
    --model.vgg_w 0.0 \
    --model.ms_ssim_w ${SLURM_ARRAY_TASK_ID} \
    --model.segmentation_model_path ./checkpoints/segmentation/version_0/last.ckpt \
    --trainer.max_epochs 200 \
    --data.rd_id 1

# hyperparameter search for munit
## ms_ssim weight
srun python main.py fit \
    --config=configs/munit.yml \
    --model.gan_w 1.0 \
    --model.recon_x_w 10.0 \
    --model.recon_s_w 1.0 \
    --model.recon_c_w 1.0 \
    --model.recon_x_cyc_w 10.0 \
    --model.ms_ssim_w ${SLURM_ARRAY_TASK_ID} \
    --model.segmentation_model_path ./checkpoints/segmentation/version_0/last.ckpt \
    --trainer.max_epochs 500 \
    --data.rd_id 1

# hyperparameter search for cut
## semantic weight
srun python main.py fit \
    --config=configs/cut.yml \
    --model.gan_w 1 \
    --model.nce_w 1 \
    --model.semantic_w ${SLURM_ARRAY_TASK_ID} \
    --model.segmentation_model_path ./checkpoints/segmentation/version_0/last.ckpt \
    --trainer.max_epochs 400 \
    --trainer.precision '32-true' \
    --data.rd_id 1
