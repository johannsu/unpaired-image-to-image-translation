# SLURM SUBMIT SCRIPT
#SBATCH -e err-Test-newB-0-%J.err
#SBATCH -o out-Test-newB-0-%J.out
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --nodes=1             # This needs to match Trainer(num_nodes=...)
#SBATCH --gpus=1
#SBATCH --ntasks-per-node=1   # This needs to match Trainer(devices=...)
#SBATCH --mem=10G
#SBATCH --signal=SIGUSR1@90

# translate using cycle_gan
srun python main.py predict \
    --config configs/cycle_gan.yml \
    --ckpt_path ./checkpoints/cycle_gan/version_0/last.ckpt \
    --data.predict_from_path ./datasets/ForceEstimationRender/raw/ \
    --data.predict_to_path ./datasets/ForceEstimationRender/translated/cycle_gan/ \
    --data.batch_size 100 \
    --data.predict_recursive true

# translate using munit
srun python main.py predict \
    --config configs/cut.yml \
    --ckpt_path ./checkpoints/munit/version_0/last.ckpt \
    --data.predict_from_path ./datasets/ForceEstimationRender/raw/ \
    --data.predict_to_path ./datasets/ForceEstimationRender/translated/munit/ \
    --data.batch_size 100 \
    --data.predict_recursive true

# translate using cut
srun python main.py predict \
    --config configs/cut.yml \
    --ckpt_path ./checkpoints/cut/version_0/last.ckpt \
    --data.predict_from_path ./datasets/ForceEstimationRender/raw/ \
    --data.predict_to_path ./datasets/ForceEstimationRender/translated/cut/ \
    --data.batch_size 100 \
    --data.predict_recursive true