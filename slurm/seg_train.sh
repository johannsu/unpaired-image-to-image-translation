#!/bin/bash -l

# SLURM SUBMIT SCRIPT
#SBATCH -e err-Test-newB-0-%J.err
#SBATCH -o out-Test-newB-0-%J.out
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --nodes=1             # This needs to match Trainer(num_nodes=...)
#SBATCH --gpus=1
#SBATCH --ntasks-per-node=1   # This needs to match Trainer(devices=...)
#SBATCH --mem=16G
#SBATCH --signal=SIGUSR1@90

# activate conda env
source /home/suessjoha/.bashrc
conda activate env

# debugging flags (optional)
export NCCL_DEBUG=INFO
export PYTHONFAULTHANDLER=1

# on your cluster you might need these:
# set the network interface
# export NCCL_SOCKET_IFNAME=^docker0,lo

# might need the latest CUDA
# module load NCCL/2.4.7-1-cuda.10.0

# segmentation model used by other models for eval during training
srun python main.py fit \
    --config configs/segmentation.yml

# Untranslated
srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode raw \
    --data.include_renders_train true \
    --data.rd_id 0 \
    --data.real_render_ratio 0.75

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode raw \
    --data.include_renders_train true \
    --data.rd_id 1 \
    --data.real_render_ratio 0.5

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode raw \
    --data.include_renders_train true \
    --data.rd_id 2 \
    --data.real_render_ratio 0.25

# Cycle gan
srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode cycle_gan \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 0 \
    --data.real_render_ratio 0.75

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode cycle_gan \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 1 \
    --data.real_render_ratio 0.5

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode cycle_gan \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 2 \
    --data.real_render_ratio 0.25

# munit
srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode munit \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 0 \
    --data.real_render_ratio 0.75

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode munit \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 1 \
    --data.real_render_ratio 0.5

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode munit \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 2 \
    --data.real_render_ratio 0.25

# cut
srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode cut \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 0 \
    --data.real_render_ratio 0.75

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode cut \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 1 \
    --data.real_render_ratio 0.5

srun python main.py fit \
    --config configs/segmentation.yml \
    --data.render_mode cut \
    --data.model_version version_0 \
    --data.include_renders_train true \
    --data.rd_id 2 \
    --data.real_render_ratio 0.25