import os
from lightning import LightningModule
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from pytorch_msssim import ms_ssim
from torch.optim import lr_scheduler
from torchmetrics.image import MultiScaleStructuralSimilarityIndexMeasure
from prettytable import PrettyTable
from torchvision.utils import save_image

from src.nn.segmentation.segmentation import Segmentation
from src.nn.utils.utils import get_mosaic
from src.nn.cut.networks import GANLoss, define_D, define_F, define_G, get_scheduler
from src.nn.cut.patchnce import PatchNCELoss

# from src.nn.eval.metrics import compute_score, distance, wasserstein, knn, mmd, inception_score, mode_score, fid


class CUT(LightningModule):
    def __init__(
        self,
        model,
        input_nc,
        output_nc,
        beta1,
        beta2,
        lr,
        gan_mode,
        pool_size,
        lr_policy,
        lr_decay_iters,
        n_epochs_decay,
        n_epochs, 
        ngf,
        ndf,
        netD,
        netG,
        n_layers_D,
        normG,
        normD,
        init_type,
        init_gain,
        no_dropout,
        no_antialias,
        no_antialias_up,
        CUT_mode,
        gan_w,
        nce_w,
        vgg_w,
        ms_ssim_w,
        # seg_w,
        nce_idt,
        nce_layers,
        nce_includes_all_negatives_from_minibatch,
        netF,
        netF_nc,
        nce_T,
        num_patches,
        flip_equivariance,
        semantic,
        semantic_w,
        cl_semantic,
        batch_size,
        log_img_every_n_steps,
        segmentation_model_path,
        predict_save_path
    ):
        super().__init__()

        self.save_hyperparameters()
        self.automatic_optimization = False

        self.nce_layers = [int(i) for i in self.hparams.nce_layers.split(',')]

        self.gen = define_G(
            input_nc=input_nc, 
            output_nc=output_nc, 
            ngf=ngf, 
            netG=netG, 
            norm=normG, 
            use_dropout=not no_dropout, 
            init_type=init_type, 
            init_gain=init_gain, 
            no_antialias=no_antialias, 
            no_antialias_up=no_antialias_up, 
            opt=self.hparams
        )

        self.netF = define_F(
            input_nc=input_nc, 
            netF=netF, 
            netF_nc=netF_nc, 
            norm=normG, 
            use_dropout=not no_dropout, 
            init_type=init_type, 
            init_gain=init_gain, 
            no_antialias=no_antialias, 
            opt=self.hparams
        )
        # self.netF_initialized = False

        self.dis = define_D(
            input_nc=output_nc, 
            ndf=ndf, 
            netD=netD, 
            n_layers_D=n_layers_D, 
            norm=normD, 
            init_type=init_type, 
            init_gain=init_gain, 
            no_antialias=no_antialias, 
            opt=self.hparams
        )

        self.opt_feat = self.scheduler_feat = None
        
        # define loss functions
        self.criterionGAN = GANLoss(gan_mode)
        self.criterionNCE = []

        self.ms_ssim = MultiScaleStructuralSimilarityIndexMeasure()

        for _ in self.nce_layers:
            self.criterionNCE.append(PatchNCELoss(self.hparams))

        self.criterionIdt = torch.nn.L1Loss()
        
        try:
            self.segmentation_model = Segmentation.load_from_checkpoint(segmentation_model_path)
            self.segmentation_model.freeze()
        except:
            print("Could not load seg model")

        if not os.path.isdir(predict_save_path):
            os.makedirs(predict_save_path)

    def forward(self, real):
        fake = self.gen(real)
        fake_b = fake[: real.size(0)]

        return fake_b

    # def forward_dev(self):
    #     real = (
    #         torch.cat((self.real_a, self.real_b), dim=0) 
    #     )

    #     # if self.hparams.flip_equivariance:
    #     #     self.flipped_for_equivariance = self.hparams.isTrain and (np.random.random() < 0.5)
    #     #     if self.flipped_for_equivariance:
    #     #         self.real = torch.flip(self.real, [3])

    #     fake = self.gen(real)
    #     fake_b = fake[: self.real_a.size(0)]
    #     idt_b = fake[self.real_a.size(0) :]

    #     return fake_b, idt_b
    def forward_dev(self, is_train=True):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.real = (
            torch.cat((self.real_a, self.real_b), dim=0) if is_train else self.real_a
        )

        self.fake = self.gen(self.real)
        self.fake_b = self.fake[: self.real_a.size(0)]
        if self.hparams.nce_idt:
            self.idt_b = self.fake[self.real_a.size(0):]

    def calculate_NCE_loss(self, src, tgt):
        n_layers = len(self.nce_layers)
        feat_q = self.gen(tgt, self.nce_layers, encode_only=True)

        feat_k = self.gen(src, self.nce_layers, encode_only=True)
        feat_k_pool, sample_ids = self.netF(feat_k, self.hparams.num_patches, None)
        feat_q_pool, _ = self.netF(feat_q, self.hparams.num_patches, sample_ids)

        total_nce_loss = 0.0
        for f_q, f_k, crit, nce_layer in zip(
            feat_q_pool, feat_k_pool, self.criterionNCE, self.nce_layers
        ):
            loss = crit(f_q, f_k) * self.hparams.nce_w
            total_nce_loss += loss.mean()

        return total_nce_loss / n_layers
    
    def calculate_semantic_loss(self, src, tgt, gt=True):
        """
        Method to compute the semantic loss

        Parameters:
        ------------------------
        src: torch.Tensor, the source image (synthetic A)
        tgt: torch.Tensor, the generated fake image
        gt: bool, whether to apply the loss on RGB or grayscale images

        Return
        ------------------------
        total_loss: float, the computed semantic loss
        """

        # compute the images' brightness
        if gt:
            src_brightness = torch.mean(src, dim=1, keepdim=True)
            tgt_brightness = torch.mean(tgt, dim=1, keepdim=True)
        else:
            src = F.rgb_to_grayscale(src)
            tgt = F.rgb_to_grayscale(tgt)
            src_brightness = torch.mean(src, dim=1, keepdim=True)
            tgt_brightness = torch.mean(tgt, dim=1, keepdim=True)
        
        total_loss = -self.ms_ssim(src_brightness, tgt_brightness) # normalize=True

        return total_loss
    
    def compute_D_loss(self):
        """Calculate GAN loss for the discriminator"""
        fake = self.fake_b.detach()
        # Fake; stop backprop to the generator by detaching fake_B
        pred_fake = self.dis(fake)
        self.loss_D_fake = self.criterionGAN(pred_fake, False).mean()
        # Real
        self.pred_real = self.dis(self.real_b)
        loss_D_real = self.criterionGAN(self.pred_real, True)
        self.loss_D_real = loss_D_real.mean()

        # combine loss and calculate gradients
        self.loss_D = (self.loss_D_fake + self.loss_D_real) * 0.5

        return self.loss_D
    
    def compute_G_loss(self):
        """Calculate GAN and NCE loss for the generator"""
        fake = self.fake_b
        # First, G(A) should fake the discriminator
        if self.hparams.nce_w > 0.0:
            pred_fake = self.dis(fake)
            self.loss_G_GAN = (
                self.criterionGAN(pred_fake, True).mean() * self.hparams.nce_w
            )
        else:
            self.loss_G_GAN = 0.0

        if self.hparams.nce_w > 0.0:
            self.loss_NCE = self.calculate_NCE_loss(self.real_a, self.fake_b)
        else:
            self.loss_NCE, self.loss_NCE_bd = 0.0, 0.0

        if self.hparams.nce_w > 0.0:
            self.loss_NCE_Y = self.calculate_NCE_loss(self.real_b, self.idt_b)
            self.loss_NCE_both = (self.loss_NCE + self.loss_NCE_Y) * 0.5
        else:
            self.loss_NCE_both = self.loss_NCE

        # structure similarity loss
        if self.hparams.semantic:
            self.loss_semantic = self.hparams.semantic_w* self.calculate_semantic_loss(self.real_a, self.fake_b, self.hparams.cl_semantic)
        else:
            self.loss_semantic = 0.0
        
        self.loss_G = self.loss_G_GAN + self.loss_NCE_both + self.loss_semantic

        return self.loss_G

    def training_step(self, batch, batch_idx):
        self.real_b, self.mask_b, self.real_a, self.mask_a = batch
        if self.current_epoch == 0 and batch_idx == 0:
            self.data_dependent_initialize()

        opt_gen, opt_dis = self.optimizers()

        self.forward_dev(True)
        # update D
        self.set_requires_grad(self.dis, True)
        opt_dis.zero_grad()
        self.loss_D = self.compute_D_loss()
        self.manual_backward(self.loss_D)
        opt_dis.step()

        # update G
        self.set_requires_grad(self.dis, False)
        opt_gen.zero_grad()
        if self.hparams.netF == "mlp_sample":
            self.opt_feat.zero_grad()
        self.loss_G = self.compute_G_loss()
        self.manual_backward(self.loss_G)
        opt_gen.step()
        if self.hparams.netF == "mlp_sample":
            self.opt_feat.step()

        # Logging
        output = {
            "loss": (self.loss_D + self.loss_G).detach(),
            "loss_G": self.loss_G.detach(),
            "loss_G_gan": self.loss_G_GAN.detach(),
            "loss_NCE_both": self.loss_NCE_both.detach(),
            "loss_semantic": self.loss_semantic.detach(),
            "loss_D": self.loss_D.detach(), 
        }

        log_dict = {"train/" + k: v for k,v in output.items()}
        self.log_dict(log_dict)

        if self.global_step % self.hparams.log_img_every_n_steps == 0:
            res = self.segmentation_model(self.fake_b)
            pred = torch.argmax(res, 1)
            grid_c = get_mosaic(
                self.mask_a.detach().cpu().numpy(),
                pred.detach().cpu().numpy(),
                # idt_.detach().cpu().numpy()
            )
            self.logger.experiment.add_image('Mask B, Predicted Mask', grid_c, global_step=int(self.global_step))

            grid_b = get_mosaic(
                self.real_a.detach().cpu().numpy(),
                self.real_b.detach().cpu().numpy(),
                self.fake_b.detach().cpu().numpy(),
                self.idt_b.detach().cpu().numpy()
            )
            self.logger.experiment.add_image('Real A, Real B, Fake B, Idt B', grid_b, global_step=int(self.global_step))

        return output

    def validation_step(self, batch):
        self.real_b, self.mask_b, self.real_a, self.mask_a = batch

        self.forward_dev(True)

        res = self.segmentation_model(self.fake_b)
        metrics = self.segmentation_model.compute_metrics(res, self.mask_a)

        output = {
            "accuracy": metrics["accuracy"],
            "precision": metrics["precision"],
            "recall": metrics["recall"],
            "jaccard_index": metrics["jaccard_index"],
            "f1_score": metrics["f1_score"], 
        }

        log_dict = {"val/" + k: v for k,v in output.items()}
        self.log_dict(log_dict)

        return output
    
    def test_step(self, batch, batch_idx):
        self.real_b, self.mask_b, self.real_a, self.mask_a = batch
        self.forward_dev(True)
        
        res = self.segmentation_model(self.fake_b)
        metrics = self.segmentation_model.compute_metrics(res, self.mask_a)

        if batch_idx == 0:
            self.test_accuracy = [metrics["accuracy"]]
            self.test_precision = [metrics["precision"]]
            self.test_recall = [metrics["recall"]]
            self.test_jaccard_index = [metrics["jaccard_index"]]
            self.test_f1_score = [metrics["f1_score"]]
            self.test_specificity = [metrics["specificity"]]
        else:
            self.test_accuracy.append(metrics["accuracy"])
            self.test_precision.append(metrics["precision"])
            self.test_recall.append(metrics["recall"])
            self.test_jaccard_index.append(metrics["jaccard_index"])
            self.test_f1_score.append(metrics["f1_score"])
            self.test_specificity.append(metrics["specificity"])
    
    def predict_step(self, batch):
        imgs, paths = batch
        res = self(imgs)

        for img, path in zip(res, paths):
            # print(img.shape)
            save_image(img, os.path.join(self.hparams.predict_save_path, path))

    def on_train_start(self, *args, **kwargs):
        # log network graph
        sampleImgA=torch.rand((1,3,256,256)).to(self.device)
        # sampleImgB=torch.rand((1,3,256,256)).to(self.device)

        self.logger.experiment.add_graph(self,[sampleImgA])

    def data_dependent_initialize(self):
        """
        The feature network netF is defined in terms of the shape of the intermediate, extracted
        features of the encoder portion of netG. Because of this, the weights of netF are
        initialized at the first feedforward pass with some input images.
        Please also see PatchSampleF.create_mlp(), which is called at the first forward() call.
        """
        self.forward_dev()  # compute fake images: G(A)
        self.compute_D_loss().backward()  # calculate gradients for D
        self.compute_G_loss().backward()  # calculate graidents for G
        self.opt_feat = torch.optim.Adam(
            self.netF.parameters(),
            lr=self.hparams.lr,
            betas=(self.hparams.beta1, self.hparams.beta2),
        )
        self.optimizers_.append(self.opt_feat)

        def lambda_rule(epoch):
            lr_l = 1.0 - max(0, epoch - self.hparams.n_epochs) / float(
                self.hparams.n_epochs_decay + 1
            )
            return lr_l

        self.schedulers_.append(
            lr_scheduler.LambdaLR(self.opt_feat, lr_lambda=lambda_rule)
        )
    
    def configure_optimizers(self):
        self.opt_gen = torch.optim.Adam(
            self.gen.parameters(), lr=self.hparams.lr, betas=(self.hparams.beta1, self.hparams.beta2)
        )
        self.opt_dis = torch.optim.Adam(
            self.dis.parameters(), lr=self.hparams.lr, betas=(self.hparams.beta1, self.hparams.beta2)
        )
        self.optimizers_ = []
        self.optimizers_.append(self.opt_gen)
        self.optimizers_.append(self.opt_dis)

        def lambda_rule(epoch):
            lr_l = 1.0 - max(0, epoch - self.hparams.n_epochs) / float(
                self.hparams.n_epochs_decay + 1
            )
            return lr_l

        self.schedulers_ = [
            # lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda_rule)
            # for optimizer in self.optimizers_
        ]

        return self.optimizers_, self.schedulers_

    def set_requires_grad(self, nets, requires_grad=False):
        """Set requies_grad=Fasle for all the networks to avoid unnecessary computations
        Parameters:
            nets (network list)   -- a list of networks
            requires_grad (bool)  -- whether the networks require gradients or not
        """
        if not isinstance(nets, list):
            nets = [nets]
        for net in nets:
            if net is not None:
                for param in net.parameters():
                    param.requires_grad = requires_grad

    def on_test_epoch_end(self):
        # Compute mean and standard deviation for each metric
        mean_accuracy = torch.mean(torch.tensor(self.test_accuracy))
        std_accuracy = torch.std(torch.tensor(self.test_accuracy))

        mean_precision = torch.mean(torch.tensor(self.test_precision))
        std_precision = torch.std(torch.tensor(self.test_precision))

        mean_recall = torch.mean(torch.tensor(self.test_recall))
        std_recall = torch.std(torch.tensor(self.test_recall))

        mean_jaccard_index = torch.mean(torch.tensor(self.test_jaccard_index))
        std_jaccard_index = torch.std(torch.tensor(self.test_jaccard_index))

        mean_f1_score = torch.mean(torch.tensor(self.test_f1_score))
        std_f1_score = torch.std(torch.tensor(self.test_f1_score))

        mean_specificity = torch.mean(torch.tensor(self.test_specificity))
        std_specificity = torch.std(torch.tensor(self.test_specificity))

        # Create a PrettyTable object
        table = PrettyTable()

        # Set column names
        table.field_names = ["Metric", "Value"]

        # Align columns: left, right
        table.align["Metric"] = "l"
        table.align["Value"] = "r"

        # Vertical alignment: middle
        table.valign["Metric"] = "m"
        table.valign["Value"] = "m"

        table.add_row(["Accuracy", f'{mean_accuracy:.2f} ± {std_accuracy:.2f}'])
        table.add_row(["Precision", f'{mean_precision:.2f} ± {std_precision:.2f}'])
        table.add_row(["Recall", f'{mean_recall:.2f} ± {std_recall:.2f}'])
        table.add_row(["Jaccard Index", f'{mean_jaccard_index:.2f} ± {std_jaccard_index:.2f}'])
        table.add_row(["F1 Score", f'{mean_f1_score:.2f} ± {std_f1_score:.2f}'])
        table.add_row(["Specificity", f'{mean_specificity:.2f} ± {std_specificity:.2f}'])

        # Print the table
        print(table)

    def load_state_dict(self, state_dict, strict=False, assign=False):
        return super().load_state_dict(state_dict, strict=False, assign=assign)