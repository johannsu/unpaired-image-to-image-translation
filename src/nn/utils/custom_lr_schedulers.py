from torch.optim.lr_scheduler import _LRScheduler

class DecayAfterDelayScheduler(_LRScheduler):
    def __init__(self, optimizer, total_epochs, last_epoch=-1):
        self.total_epochs = total_epochs
        super(DecayAfterDelayScheduler, self).__init__(optimizer, last_epoch)

    def get_lr(self):
        current_epoch = self.last_epoch + 1
        if current_epoch < 100:
            return [base_lr for base_lr in self.base_lrs]
        else:
            decay_factor = (self.total_epochs - current_epoch) / (self.total_epochs - 100)
            return [base_lr * decay_factor for base_lr in self.base_lrs]