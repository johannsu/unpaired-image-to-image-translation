import numpy as np


def set_requires_grad(nets, requires_grad=False):
    """Set requies_grad=Fasle for all the networks to avoid unnecessary computations
    Parameters:
        nets (network list)   -- a list of networks
        requires_grad (bool)  -- whether the networks require gradients or not
    """
    if not isinstance(nets, list):
        nets = [nets]
    for net in nets:
        if net is not None:
            for param in net.parameters():
                param.requires_grad = requires_grad

def get_mosaic(*args):
    stacks = []
    for i in range(len(args)):
        if type(args[i]) is not np.ndarray:
            continue
        
        if len(args[i].shape) == 3:
            batch = np.expand_dims(args[i], axis=1)
            batch = np.repeat(batch, 3, axis=1)
        else:
            batch = args[i]

        batch = np.transpose(batch, (0, 2, 3, 1))
        stack = np.vstack([batch[i] for i in range(batch.shape[0])])
        stacks.append(stack)

    mosaic = np.hstack([*stacks])
    mosaic = np.transpose(mosaic, (2,0,1))

    return mosaic