import os
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models import vgg16, VGG16_Weights
from lightning import LightningModule
from pytorch_msssim import ms_ssim
from torchmetrics.image import MultiScaleStructuralSimilarityIndexMeasure
# from torchmetrics.image.fid import FrechetInceptionDistance
# from torchmetrics.image.kid import KernelInceptionDistance
from prettytable import PrettyTable
from torchvision.utils import save_image

from src.nn.segmentation.segmentation import Segmentation
from src.nn.utils.utils import get_mosaic
from src.nn.munit.networks import AdaINGen, MsImageDis, StylelessGen


class MUNIT(LightningModule):
    def __init__(
        self, 
        lr,
        weight_decay,
        beta1,
        beta2,
        lr_policy,
        step_size,
        gen_steps_per_iter,
        gamma,
        gan_w,
        recon_x_w,
        recon_s_w,
        recon_c_w,
        recon_x_cyc_w,
        vgg_w,
        ms_ssim_w,
        # seg_w,
        input_dim_a,
        input_dim_b,
        gen,
        dis,
        display_size,
        log_img_every_n_steps,
        vgg_model_path,
        segmentation_model_path,
        predict_save_path
    ):
        super().__init__()

        self.save_hyperparameters()
        self.automatic_optimization = False

        self.gen_a = AdaINGen(input_dim_a, gen)  # auto-encoder for domain a
        # self.gen_a = StylelessGen(input_dim_b, gen)
        self.gen_b = AdaINGen(input_dim_b, gen)  # auto-encoder for domain b
        self.dis_a = MsImageDis(input_dim_a, dis)  # discriminator for domain a
        self.dis_b = MsImageDis(input_dim_b, dis)  # discriminator for domain b
        self.instancenorm = nn.InstanceNorm2d(512, affine=False)
        self.style_dim = gen['style_dim']

        # fix the noise used in sampling
        display_size = int(display_size)
        self.register_buffer("rand_style_a", torch.randn(display_size, self.style_dim, 1, 1))
        self.register_buffer("rand_style_b", torch.randn(display_size, self.style_dim, 1, 1))

        self.ms_ssim = MultiScaleStructuralSimilarityIndexMeasure()

        # Network weight initialization
        # self.apply(weights_init(hyperparameters['init']))
        # self.dis_a.apply(weights_init('gaussian'))
        # self.dis_b.apply(weights_init('gaussian'))

        # Load VGG model if needed
        if 'vgg_w' in self.hparams.keys() and self.hparams.vgg_w > 0:
            weights = VGG16_Weights.IMAGENET1K_FEATURES
            self.vgg = vgg16(weights).features[:23].eval()

            self.vgg_preprocess = weights.transforms()
            for param in self.vgg.parameters():
                param.requires_grad = False


        try:
            self.segmentation_model = Segmentation.load_from_checkpoint(segmentation_model_path)
            self.segmentation_model.freeze()
        except:
            print("Could not load seg model")

        # self.fid = FrechetInceptionDistance(feature=64, input_img_size=(3,640,512), normalize=True)
        # self.kid = KernelInceptionDistance(feature=64, input_img_size=(3,640,512))

    def forward(self, real):
        # encode real
        content_a, style_a = self.gen_a.encode(real)
        # content_a = self.gen_a.encode(real_a)
        # content_b, style_b = self.gen_b.encode(real_b)

        # decode (cross domain)
        # x_ba = self.gen_a.decode(content_b, self.rand_style_a)
        # x_ba = self.gen_a.decode(content_b) 
        x_ab = self.gen_b.decode(content_a, self.rand_style_b)

        return x_ab#, x_ba

    def forward_dev(self, real_a, real_b):
        # encode real
        content_a, style_a = self.gen_a.encode(real_a)
        # content_a = self.gen_a.encode(real_a)
        content_b, style_b = self.gen_b.encode(real_b)

        # decode (cross domain)
        x_ba = self.gen_a.decode(content_b, self.rand_style_a)
        # x_ba = self.gen_a.decode(content_b) 
        x_ab = self.gen_b.decode(content_a, self.rand_style_b)

        return x_ab, x_ba
    
    def recon_criterion(self, input, target):
        return torch.mean(torch.abs(input - target))
    
    def compute_perceptual_loss(self, img, target):
        img_vgg = self.vgg_preprocess(img)
        target_vgg = self.vgg_preprocess(target)

        img_fea = self.vgg(img_vgg)
        target_fea = self.vgg(target_vgg)

        return torch.mean((self.instancenorm(img_fea) - self.instancenorm(target_fea)) ** 2)

    def dis_step(self, real_a, real_b, split):
        # Train Discriminator
        if split == 'train':
            opt_disc, _ = self.optimizers()
            opt_disc.zero_grad()

        style_a = torch.randn(real_a.size(0), self.style_dim, 1, 1, device=self.device)
        style_b = torch.randn(real_b.size(0), self.style_dim, 1, 1, device=self.device)

        # encode
        # c_a = self.gen_a.encode(real_a) 
        c_a, _ = self.gen_a.encode(real_a)
        c_b, _ = self.gen_b.encode(real_b)

        # decode (cross domain)
        x_ba = self.gen_a.decode(c_b, style_a)
        # x_ba = self.gen_a.decode(c_b)
        x_ab = self.gen_b.decode(c_a, style_b)

        # Discriminator loss
        loss_dis_a = self.dis_a.calc_dis_loss(x_ba, real_a)
        loss_dis_b = self.dis_b.calc_dis_loss(x_ab, real_b)
        loss_dis_total = self.hparams.gan_w * loss_dis_a + \
                         self.hparams.gan_w * loss_dis_b
        
        if split == "train":
            self.manual_backward(loss_dis_total, retain_graph=True)
            opt_disc.step()

        # Logging
        output = {
            "loss_D": loss_dis_total.detach(),
            "loss_D_a": loss_dis_a.detach(), 
            "loss_D_b": loss_dis_b.detach(), 
        }

        return output

    def gen_step(
        self, 
        real_a, 
        real_b,
        mask_a,
        mask_b, 
        split
    ):
        # Train Generator
        if split == 'train':
            _, opt_gen = self.optimizers()
            opt_gen.zero_grad()

        style_a = torch.randn(real_a.size(0), self.style_dim, 1, 1, device=self.device)
        style_b = torch.randn(real_b.size(0), self.style_dim, 1, 1, device=self.device)
        
        # encode
        content_a, style_a_prime = self.gen_a.encode(real_a)
        # content_a = self.gen_a.encode(real_a)
        content_b, style_b_prime = self.gen_b.encode(real_b)

        # decode (within domain)
        a_recon = self.gen_a.decode(content_a, style_a_prime)
        # a_recon = self.gen_a.decode(content_a)
        b_recon = self.gen_b.decode(content_b, style_b_prime)

        # decode (cross domain)
        x_ba = self.gen_a.decode(content_b, style_a)
        # x_ba = self.gen_a.decode(content_b)
        x_ab  = self.gen_b.decode(content_a, style_b)

        # encode again
        c_b_recon, s_a_recon = self.gen_a.encode(x_ba)
        # c_b_recon = self.gen_a.encode(x_ba) 
        c_a_recon, s_b_recon = self.gen_b.encode(x_ab)

        # decode again (if needed)
        cycle_a = self.gen_a.decode(c_a_recon, style_a_prime) if self.hparams.recon_x_cyc_w > 0 else None
        # cycle_a = self.gen_a.decode(c_a_recon) if self.hparams.recon_x_cyc_w > 0 else None
        cycle_b = self.gen_b.decode(c_b_recon, style_b_prime) if self.hparams.recon_x_cyc_w > 0 else None

        # reconstruction loss
        loss_gen_recon_a = self.recon_criterion(a_recon, real_a)
        loss_gen_recon_b = self.recon_criterion(b_recon, real_b)
        loss_gen_recon_s_a = self.recon_criterion(s_a_recon, style_a)
        loss_gen_recon_s_b = self.recon_criterion(s_b_recon, style_b)
        loss_gen_recon_c_a = self.recon_criterion(c_a_recon, content_a)
        loss_gen_recon_c_b = self.recon_criterion(c_b_recon, content_b)
        loss_gen_cycrecon_x_a = self.recon_criterion(cycle_a, real_a) if self.hparams.recon_x_cyc_w > 0 else torch.tensor(0, dtype=torch.float32)
        loss_gen_cycrecon_x_b = self.recon_criterion(cycle_b, real_b) if self.hparams.recon_x_cyc_w > 0 else torch.tensor(0, dtype=torch.float32)

        # GAN loss
        loss_gen_adv_a = self.dis_a.calc_gen_loss(x_ba)
        loss_gen_adv_b = self.dis_b.calc_gen_loss(x_ab)

        # Structural similarity:
        if self.hparams.ms_ssim_w > 0:
            x_a_brightness = torch.mean(real_a, dim=1, keepdim=True)
            x_b_brightness = torch.mean(real_b, dim=1, keepdim=True)
            x_ab_brightness = torch.mean(x_ab, dim=1, keepdim=True)
            x_ba_brightness = torch.mean(x_ba, dim=1, keepdim=True)

            loss_msssim_ab = -self.ms_ssim(x_a_brightness, x_ab_brightness)
            loss_msssim_ba = -self.ms_ssim(x_b_brightness, x_ba_brightness)
        else:
            loss_msssim_ab = loss_msssim_ba = torch.tensor(0, dtype=torch.float32) 

        # domain-invariant perceptual loss
        loss_gen_vgg_a = self.compute_perceptual_loss(x_ba, real_b) if self.hparams.vgg_w > 0 else torch.tensor(0, dtype=torch.float32)
        loss_gen_vgg_b = self.compute_perceptual_loss(x_ab, real_a) if self.hparams.vgg_w > 0 else torch.tensor(0, dtype=torch.float32)

        # total generator loss
        loss_gen_total = self.hparams.gan_w * loss_gen_adv_a + \
                         self.hparams.gan_w * loss_gen_adv_b + \
                         self.hparams.recon_x_w * loss_gen_recon_a + \
                         self.hparams.recon_s_w * loss_gen_recon_s_a + \
                         self.hparams.recon_c_w * loss_gen_recon_c_a + \
                         self.hparams.recon_x_w * loss_gen_recon_b + \
                         self.hparams.recon_s_w * loss_gen_recon_s_b + \
                         self.hparams.recon_c_w * loss_gen_recon_c_b + \
                         self.hparams.recon_x_cyc_w * loss_gen_cycrecon_x_a + \
                         self.hparams.recon_x_cyc_w * loss_gen_cycrecon_x_b + \
                         self.hparams.ms_ssim_w * loss_msssim_ab + \
                         self.hparams.ms_ssim_w * loss_msssim_ba + \
                         self.hparams.vgg_w * loss_gen_vgg_a + \
                         self.hparams.vgg_w * loss_gen_vgg_b

        if split == "train":
            self.manual_backward(loss_gen_total)
            opt_gen.step()

        # self.fid.update(real_a, True)
        # self.fid.update(x_ba, False)

        # self.kid.update(real_a, True)
        # self.kid.update(x_ba, False)

        # Logging
        # fid_score = 0
        # try:
        #     fid_score = self.fid.compute().detach()
        # except RuntimeError:
        #     pass

        output = {
            "loss_G": loss_gen_total.detach(),
            "loss_gen_a": (
                self.hparams.gan_w * loss_gen_adv_a + \
                self.hparams.recon_x_w * loss_gen_recon_a + \
                self.hparams.recon_s_w * loss_gen_recon_s_a + \
                self.hparams.recon_c_w * loss_gen_recon_c_a + \
                self.hparams.recon_x_cyc_w * loss_gen_cycrecon_x_a
            ).detach(),
            "loss_gen_b": (
                self.hparams.gan_w * loss_gen_adv_b + \
                self.hparams.recon_x_w * loss_gen_recon_b + \
                self.hparams.recon_s_w * loss_gen_recon_s_b + \
                self.hparams.recon_c_w * loss_gen_recon_c_b + \
                self.hparams.recon_x_cyc_w * loss_gen_cycrecon_x_b
            ).detach(),
            "loss_gen_adv_a": (self.hparams.gan_w * loss_gen_adv_a).detach(), 
            "loss_gen_recon_a": (self.hparams.recon_x_w * loss_gen_recon_a).detach(), 
            "loss_gen_recon_s_a": (self.hparams.recon_s_w * loss_gen_recon_s_a).detach(), 
            "loss_gen_recon_c_a": (self.hparams.recon_c_w * loss_gen_recon_c_a).detach(),
            "loss_gen_cycrecon_x_a": (self.hparams.recon_x_cyc_w * loss_gen_cycrecon_x_a).detach(),
            "loss_gen_adv_b": (self.hparams.gan_w * loss_gen_adv_b).detach(), 
            "loss_gen_recon_b": (self.hparams.recon_x_w * loss_gen_recon_b).detach(), 
            "loss_gen_recon_s_b": (self.hparams.recon_s_w * loss_gen_recon_s_b).detach(), 
            "loss_gen_recon_c_b": (self.hparams.recon_c_w * loss_gen_recon_c_b).detach(),
            "loss_gen_cycrecon_x_b": (self.hparams.recon_x_cyc_w * loss_gen_cycrecon_x_b).detach(),
            "loss_gen_msssim_ab": (self.hparams.ms_ssim_w * loss_msssim_ab).detach(),
            "loss_gen_msssim_ba": (self.hparams.ms_ssim_w * loss_msssim_ba).detach(),
            "loss_gen_vgg_a": (self.hparams.vgg_w * loss_gen_vgg_a).detach(),
            "loss_gen_vgg_b": (self.hparams.vgg_w * loss_gen_vgg_a).detach(),
            # "fid_score": fid_score 
        }

        if split == 'val':
            res = self.segmentation_model(x_ba)
            metrics = self.segmentation_model.compute_metrics(res, mask_b)

            output = {
                **output,
                "accuracy": metrics["accuracy"],
                "precision": metrics["precision"],
                "recall": metrics["recall"],
                "jaccard_index": metrics["jaccard_index"],
                "f1_score": metrics["f1_score"],
            }

        if self.global_step % self.hparams.log_img_every_n_steps == 0:
            res = self.segmentation_model(x_ba)
            pred = torch.argmax(res, 1)
            
            if self.hparams.recon_x_cyc_w > 0:
                grid_a = get_mosaic(
                    real_a.detach().cpu().numpy(),
                    a_recon.detach().cpu().numpy(),
                    x_ab.detach().cpu().numpy(),
                    cycle_a.detach().cpu().numpy()
                )
                grid_b = get_mosaic(
                    real_b.detach().cpu().numpy(),
                    b_recon.detach().cpu().numpy(),
                    x_ba.detach().cpu().numpy(),
                    cycle_b.detach().cpu().numpy()
                )
                grid_c = get_mosaic(
                    mask_b.detach().cpu().numpy(),
                    pred.detach().cpu().numpy(),
                )

                self.logger.experiment.add_image('Real A, Recon A, Fake A, Cycle A', grid_a, global_step=int(self.global_step))
                self.logger.experiment.add_image('Real B, Recon B, Fake B, Cycle B', grid_b, global_step=int(self.global_step))
                self.logger.experiment.add_image('Mask, Predicted Mask', grid_c, global_step=int(self.global_step))
            else:
                grid_a = get_mosaic(
                    real_a.detach().cpu().numpy(),
                    a_recon.detach().cpu().numpy(),
                    x_ab.detach().cpu().numpy()
                )
                grid_b = get_mosaic(
                    real_b.detach().cpu().numpy(),
                    b_recon.detach().cpu().numpy(),
                    x_ba.detach().cpu().numpy()
                )
                grid_c = get_mosaic(
                    mask_b.detach().cpu().numpy(),
                    pred.detach().cpu().numpy(),
                )

                self.logger.experiment.add_image('Real A, Recon A, x_ab', grid_a, global_step=int(self.global_step))
                self.logger.experiment.add_image('Real B, Recon B, x_ba', grid_b, global_step=int(self.global_step))
                self.logger.experiment.add_image('Mask, Predicted Mask', grid_c, global_step=int(self.global_step))

        return output

    def shared_step(self, batch, split):
        real_a, mask_a, real_b, mask_b = batch
        loss_dis = self.dis_step(real_a, real_b, split)
        
        loss_gen = self.gen_step(
            real_a, 
            real_b,
            mask_a,
            mask_b,
            split
        )

        # Logging
        output = {
            "loss": (loss_dis["loss_D"] + loss_gen["loss_G"]).detach(),
            **loss_gen,
            **loss_dis,
        }

        log_dict = {split + "/" + k: v for k,v in output.items()}
        self.log_dict(log_dict)

        return output

    def training_step(self, batch):
        return self.shared_step(batch, 'train')

    def validation_step(self, batch):
        return self.shared_step(batch, 'val')
    
    def test_step(self, batch, batch_idx):
        real_a, mask_a, real_b, mask_b = batch
        x_ab, x_ba = self.forward_dev(real_a, real_b)
        
        res = self.segmentation_model(x_ba)
        metrics = self.segmentation_model.compute_metrics(res, mask_b)

        if batch_idx == 0:
            self.test_accuracy = [metrics["accuracy"]]
            self.test_precision = [metrics["precision"]]
            self.test_recall = [metrics["recall"]]
            self.test_jaccard_index = [metrics["jaccard_index"]]
            self.test_f1_score = [metrics["f1_score"]]
            self.test_specificity = [metrics["specificity"]]
        else:
            self.test_accuracy.append(metrics["accuracy"])
            self.test_precision.append(metrics["precision"])
            self.test_recall.append(metrics["recall"])
            self.test_jaccard_index.append(metrics["jaccard_index"])
            self.test_f1_score.append(metrics["f1_score"])
            self.test_specificity.append(metrics["specificity"])

    def predict_step(self, batch, batch_idx):
        imgs, paths = batch
        res = self(imgs)

        for img, path in zip(res, paths):
            save_image(img, os.path.join(self.hparams.predict_save_path, path))
    
    def on_train_start(self, *args, **kwargs):
        # log network graph
        sampleImgA=torch.rand((1,3,256,256)).to(self.device)
        # sampleImgB=torch.rand((1,3,256,256)).to(self.device)

        self.logger.experiment.add_graph(self,[sampleImgA])

    def on_train_epoch_end(self, *args, **kwargs):
        scheduler_G, scheduler_D = self.lr_schedulers()
        scheduler_G.step()
        scheduler_D.step()

    def on_test_epoch_end(self):
        # Compute mean and standard deviation for each metric
        mean_accuracy = torch.mean(torch.tensor(self.test_accuracy))
        std_accuracy = torch.std(torch.tensor(self.test_accuracy))

        mean_precision = torch.mean(torch.tensor(self.test_precision))
        std_precision = torch.std(torch.tensor(self.test_precision))

        mean_recall = torch.mean(torch.tensor(self.test_recall))
        std_recall = torch.std(torch.tensor(self.test_recall))

        mean_jaccard_index = torch.mean(torch.tensor(self.test_jaccard_index))
        std_jaccard_index = torch.std(torch.tensor(self.test_jaccard_index))

        mean_f1_score = torch.mean(torch.tensor(self.test_f1_score))
        std_f1_score = torch.std(torch.tensor(self.test_f1_score))

        mean_specificity = torch.mean(torch.tensor(self.test_specificity))
        std_specificity = torch.std(torch.tensor(self.test_specificity))

        # Create a PrettyTable object
        table = PrettyTable()

        # Set column names
        table.field_names = ["Metric", "Value"]

        # Align columns: left, right
        table.align["Metric"] = "l"
        table.align["Value"] = "r"

        # Vertical alignment: middle
        table.valign["Metric"] = "m"
        table.valign["Value"] = "m"

        table.add_row(["Accuracy", f'{mean_accuracy:.2f} ± {std_accuracy:.2f}'])
        table.add_row(["Precision", f'{mean_precision:.2f} ± {std_precision:.2f}'])
        table.add_row(["Recall", f'{mean_recall:.2f} ± {std_recall:.2f}'])
        table.add_row(["Jaccard Index", f'{mean_jaccard_index:.2f} ± {std_jaccard_index:.2f}'])
        table.add_row(["F1 Score", f'{mean_f1_score:.2f} ± {std_f1_score:.2f}'])
        table.add_row(["Specificity", f'{mean_specificity:.2f} ± {std_specificity:.2f}'])

        # Print the table
        print(table)

    def configure_optimizers(self):
        opt_disc = torch.optim.Adam(
            list(self.dis_a.parameters()) + list(self.dis_b.parameters()),
            lr=self.hparams.lr, 
            betas=(self.hparams.beta1, self.hparams.beta2), 
            weight_decay=self.hparams.weight_decay
        )
        opt_gen = torch.optim.Adam(
            list(self.gen_a.parameters()) + list(self.gen_b.parameters()),
            lr=self.hparams.lr,
            betas=(self.hparams.beta1, self.hparams.beta2), 
            weight_decay=self.hparams.weight_decay
        )

        # Does nothing - placeholder
        # scheduler_disc = torch.optim.lr_scheduler.LambdaLR(opt_disc, lr_lambda=lambda epoch: 1.0)
        # scheduler_gen = torch.optim.lr_scheduler.LambdaLR(opt_gen, lr_lambda=lambda epoch: 1.0)
        
        scheduler_disc = torch.optim.lr_scheduler.StepLR(
            opt_disc, 
            step_size=self.hparams.step_size,
            gamma=self.hparams.gamma, 
            last_epoch=-1
        )
        scheduler_gen = torch.optim.lr_scheduler.StepLR(
            opt_gen, 
            step_size=self.hparams.step_size,
            gamma=self.hparams.gamma, 
            last_epoch=-1
        )

        return [opt_disc, opt_gen], [scheduler_disc, scheduler_gen]

    def load_state_dict(self, state_dict, strict=False, assign=False):
        return super().load_state_dict(state_dict, strict=False, assign=assign)