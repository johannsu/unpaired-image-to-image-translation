from lightning import LightningModule
from torchmetrics.classification import BinaryAccuracy, BinaryPrecision, BinaryRecall, BinaryJaccardIndex, BinaryF1Score, BinarySpecificity
from transformers import SegformerForSemanticSegmentation
from torchvision.models.vgg import VGG11_Weights
from torchvision.models.segmentation import deeplabv3_resnet50
from torchvision.models.segmentation.deeplabv3 import DeepLabHead, DeepLabV3_ResNet50_Weights
import torch
import torch.nn as nn
from prettytable import PrettyTable
from torchvision.utils import save_image
import csv

from src.nn.utils.utils import get_mosaic
from src.nn.segmentation.models import UNet11


class Segmentation(LightningModule):
    def __init__(
            self,
            model_name,
            num_classes,
            lr
        ):
        super().__init__()

        self.save_hyperparameters()

        if model_name == 'unet':
            self.model = UNet11(num_classes=num_classes, weights=VGG11_Weights.DEFAULT)
        elif model_name == 'segformer':
            self.model = SegformerForSemanticSegmentation.from_pretrained("nvidia/segformer-b3-finetuned-cityscapes-1024-1024",num_labels=num_classes,ignore_mismatched_sizes=True)
        elif model_name == 'deeplab':
            self.model = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT, progress=True)
            self.model.classifier = DeepLabHead(2048, num_classes)
        else:
            raise Exception("Specified model is not available. Please use one of [unet, segformer, deeplab]")
        
        # weights = np.zeros(num_classes, dtype=np.float32)
        self.criterion = nn.CrossEntropyLoss() # weight=torch.Tensor(weights)
    
        self.acc = BinaryAccuracy()
        self.pr = BinaryPrecision()
        self.rc = BinaryRecall()
        self.jac = BinaryJaccardIndex()
        self.f1 = BinaryF1Score()
        self.spec = BinarySpecificity()
        # self.cm = BinaryConfusionMatrix()

    def forward(self, img):
        outputs = self.model(img)
        if self.hparams.model_name == 'unet':
           out = outputs
        elif self.hparams.model_name == 'segformer':
            out = nn.functional.interpolate(outputs["logits"], size=img.shape[-2:], mode="bilinear", align_corners=False)
        elif self.hparams.model_name == 'deeplab':
            out = outputs['out']
        else:
            raise Exception("Expected model_name to be one of [unet, segformer, deeplab]")
        
        return out
    
    def compute_metrics(self, out, label):
        pred = torch.argmax(out, 1)
        return {
            "accuracy": self.acc(pred, label).detach().cpu(),
            "precision": self.pr(pred, label).detach().cpu(),
            "recall": self.rc(pred, label).detach().cpu(),
            "jaccard_index": self.jac(pred, label).detach().cpu(),
            "f1_score": self.f1(pred, label).detach().cpu(),
            "specificity": self.spec(pred, label).detach().cpu()
        }

    def shared_step(self, batch, split):
        img, label = batch
        out = self.forward(img)

        loss = self.criterion(out, label.long())
        pred = torch.argmax(out, 1)

        metrics = self.compute_metrics(out, label)

        output = {
            "loss": loss,
            **metrics
        }

        log_dict = {split + "/" + k: v for k,v in output.items()}
        self.log_dict(log_dict)

        if self.global_step % 10 == 0:
            grid = get_mosaic(
                img.detach().cpu().numpy(), 
                label.detach().cpu().numpy(),
                pred.detach().cpu().numpy(),
            )
            self.logger.experiment.add_image('Img, Label, Pred', grid, global_step=int(self.global_step))

        return output

    def training_step(self, batch):
        return self.shared_step(batch, 'train')

    def validation_step(self, batch):
        return self.shared_step(batch, 'val')
    
    def test_step(self, batch, batch_idx):
        img, label = batch
        out = self.forward(img)
        pred = torch.argmax(out, 1)

        grid = get_mosaic(
            img.detach().cpu().numpy(), 
            label.detach().cpu().numpy(),
            pred.detach().cpu().numpy(),
        )
        save_image(torch.tensor(grid), f"test/{batch_idx:02d}.png")

        metrics = self.compute_metrics(out, label)

        if batch_idx == 0:
            self.test_accuracy = [metrics["accuracy"]]
            self.test_precision = [metrics["precision"]]
            self.test_recall = [metrics["recall"]]
            self.test_jaccard_index = [metrics["jaccard_index"]]
            self.test_f1_score = [metrics["f1_score"]]
            self.test_specificity = [metrics["specificity"]]
        else:
            self.test_accuracy.append(metrics["accuracy"])
            self.test_precision.append(metrics["precision"])
            self.test_recall.append(metrics["recall"])
            self.test_jaccard_index.append(metrics["jaccard_index"])
            self.test_f1_score.append(metrics["f1_score"])
            self.test_specificity.append(metrics["specificity"])

        # output = metrics

        # log_dict = {"test/" + k: v for k,v in output.items()}
        # self.log_dict(log_dict)

        # return output 
    
    def predict_step(self, batch, batch_idx):
        pass

    def on_train_epoch_end(self, *args, **kwargs):
        scheduler= self.lr_schedulers()
        scheduler.step()

    def on_test_epoch_end(self):
        # Write the metrics to a CSV file
        with open('test_metrics.csv', mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["accuracy", "precision", "recall", "jaccard_index", "f1_score"])
            writer.writerows(zip(*[
                [x.item() for x in self.test_accuracy], 
                [x.item() for x in self.test_precision], 
                [x.item() for x in self.test_recall], 
                [x.item() for x in self.test_jaccard_index], 
                [x.item() for x in self.test_f1_score]
            ]))

        # Compute mean and standard deviation for each metric
        mean_accuracy = torch.mean(torch.tensor(self.test_accuracy))
        std_accuracy = torch.std(torch.tensor(self.test_accuracy))

        mean_precision = torch.mean(torch.tensor(self.test_precision))
        std_precision = torch.std(torch.tensor(self.test_precision))

        mean_recall = torch.mean(torch.tensor(self.test_recall))
        std_recall = torch.std(torch.tensor(self.test_recall))

        mean_jaccard_index = torch.mean(torch.tensor(self.test_jaccard_index))
        std_jaccard_index = torch.std(torch.tensor(self.test_jaccard_index))

        mean_f1_score = torch.mean(torch.tensor(self.test_f1_score))
        std_f1_score = torch.std(torch.tensor(self.test_f1_score))

        mean_specificity = torch.mean(torch.tensor(self.test_specificity))
        std_specificity = torch.std(torch.tensor(self.test_specificity))

        # Create a PrettyTable object
        table = PrettyTable()

        # Set column names
        table.field_names = ["Metric", "Value"]

        # Align columns: left, right
        table.align["Metric"] = "l"
        table.align["Value"] = "r"

        # Vertical alignment: middle
        table.valign["Metric"] = "m"
        table.valign["Value"] = "m"

        table.add_row(["Accuracy", f'{mean_accuracy:.2f} ± {std_accuracy:.2f}'])
        table.add_row(["Precision", f'{mean_precision:.2f} ± {std_precision:.2f}'])
        table.add_row(["Recall", f'{mean_recall:.2f} ± {std_recall:.2f}'])
        table.add_row(["Jaccard Index", f'{mean_jaccard_index:.2f} ± {std_jaccard_index:.2f}'])
        table.add_row(["F1 Score", f'{mean_f1_score:.2f} ± {std_f1_score:.2f}'])
        table.add_row(["Specificity", f'{mean_specificity:.2f} ± {std_specificity:.2f}'])

        # Print the table
        print(table)

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(filter(lambda p: p.requires_grad, self.model.parameters()), lr=self.hparams.lr, weight_decay=1e-1)#, weight_decay=0.001)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 10, 0.9, verbose=True)

        return {"optimizer": optimizer, "lr_scheduler": lr_scheduler}