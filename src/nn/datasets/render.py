import os
import sys
import math
from uu import decode

from torchvision.io import ImageReadMode, decode_image
from torchvision.transforms import Resize
from torch.utils.data import Dataset
import cv2
from tqdm import tqdm

from src.scene.bowel import BowelScene


class RenderDataset(Dataset):
    def __init__(
        self,
        root,
        id=0,
        size=None,
        transform=None,
        image_size=None,
        generate=False,
        simple_mat=False,
        simple_shape=False
    ):
        super().__init__()

        self.root = root
        self.id = id
        if size == None:
            self.size = len(list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder))))
        else:
            self.size = size
        if self.size <= 0:
            raise ValueError("Dataset size must be > 0")
        self.transform = transform
        self.image_size = image_size
        self.simple_mat = simple_mat
        self.simple_shape = simple_shape

        if generate:
            self.generate()

        self.img_paths = list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder)))
    
    @property
    def raw_folder(self) -> str:
        return os.path.join(self.root, self.__class__.__name__, "raw", f"{self.id:02d}")

    def __len__(self):
        return self.size

    def __getitem__(self, idx):
        path = os.path.join(self.raw_folder, self.img_paths[idx])
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if self.image_size is not None:
            img = cv2.resize(img, self.image_size)

        mask_file = path.replace("img", "msk")
        mask_orig = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
        
        if self.image_size is not None:
            mask_orig = cv2.resize(mask_orig,self.image_size, interpolation=cv2.INTER_NEAREST)

        mask = (mask_orig > 0)*1

        if not self.transform is None:
            transformed = self.transform(image=img, mask=mask)

            img = transformed["image"]
            mask = transformed["mask"]
      
        return img, mask

    def _check_exists(self):
        return os.path.exists(self.raw_folder) and len(list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder)))) >= self.size

    def generate(self):
        if self._check_exists():
            self.img_paths = list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder)))
            return
        
        os.makedirs(self.raw_folder, exist_ok=True)
        
        total = self.size - len(list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder))))
        
        img_per_scene = 10

        with tqdm(total=math.ceil(total / img_per_scene)*img_per_scene, desc="Rendering images") as pb:
            for i in range(math.ceil(total / img_per_scene)):
                # redirect output to log file
                logfile = 'blender_render.log'
                open(logfile, 'a').close()
                old = os.dup(sys.stdout.fileno())
                sys.stdout.flush()
                os.close(sys.stdout.fileno())
                fd = os.open(logfile, os.O_WRONLY)

                scene = BowelScene(seed=i, save_blend_file=False, simple_mat=self.simple_mat, simple_shape=self.simple_shape)
                scene.render(img_per_scene, res=self.image_size, path=self.raw_folder, create_masks=True, progress_bar=pb)

                # disable output redirection
                os.close(fd)
                os.dup(old)
                os.close(old)

        self.img_paths = list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder)))