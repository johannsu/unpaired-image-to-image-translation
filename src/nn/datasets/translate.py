import os
from torch.utils.data import Dataset
import cv2

class TranslateDataset(Dataset):
    def __init__(
        self,
        from_path,
        to_path,
        recursive=True,
        image_size=None,
        transform=None
    ):
        super().__init__()

        self.from_path = from_path
        self.to_path = to_path
        self.recursive = recursive
        self.image_size = image_size
        self.transform = transform
        
        self.img_paths = []
        if recursive:
            for (root,dirs,files) in os.walk(self.from_path,topdown=True):
                if any(".png" in x for x in files):
                    for f in filter(lambda x: ".png" in x, files):
                        # print(os.path.join(root[len(self.from_path):], f))
                        # dirpath = os.path.join(str(f).split("/")[:-1])
                        os.makedirs(os.path.join(to_path, root[len(self.from_path):]), exist_ok=True)
                        self.img_paths.append(os.path.join(root[len(self.from_path):], f))
        else:
            self.img_paths = list(filter(lambda x: ".png" in x, os.listdir(self.from_path)))

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, idx):
        path = os.path.join(self.from_path, self.img_paths[idx])
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if self.image_size is not None:
            img = cv2.resize(img, self.image_size)

        if not self.transform is None:
            transformed = self.transform(image=img)

            img = transformed["image"]
      
        return img, self.img_paths[idx]