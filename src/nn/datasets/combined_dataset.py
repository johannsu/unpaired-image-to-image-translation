from torch.utils.data import Dataset

class CombinedDataset(Dataset):
    def __init__(
            self,
            *datasets: list[Dataset]
    ):
        super().__init__()
        self.datasets = datasets

    def __len__(self):
        return min(len(ds) for ds in self.datasets)
    
    def __getitem__(self, idx):
        return tuple(e for ds in self.datasets for e in ds[idx])