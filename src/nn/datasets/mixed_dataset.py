from torch.utils.data import Dataset
import numpy as np
import math

class MixedDataset(Dataset):
    def __init__(
            self,
            *datasets: list[Dataset],
            percentage: list[float]
    ):
        assert len(datasets) == len(percentage), "Number of datasets and percentages must match"
        assert np.isclose(sum(percentage), 1.0), "Percentages must sum to 1"

        self.datasets = datasets
        self.percentage = percentage

        # Calculate the number of samples from each dataset
        self.lengths = [math.floor(len(dataset) * p) for dataset, p in zip(datasets, percentage)]
        self.cumulative_lengths = np.cumsum(self.lengths)

    def __len__(self):
        return sum(self.lengths)

    def __getitem__(self, idx):
        # Determine which dataset the index belongs to
        dataset_idx = np.searchsorted(self.cumulative_lengths, idx, side='right')
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_lengths[dataset_idx - 1]
        
        return self.datasets[dataset_idx][sample_idx]