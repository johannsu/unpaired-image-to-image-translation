import os
from lightning import LightningDataModule
import torch
from torch.utils.data import DataLoader, random_split

from src.nn.datasets.translate import TranslateDataset
from src.nn.datasets.combined_dataset import CombinedDataset
from src.nn.datasets.render import RenderDataset
from src.nn.datasets.dsad import DSAD


class GanDataset(LightningDataModule):
    def __init__(
        self,
        num_workers,
        batch_size,
        pin_memory,
        root,
        dsad_organ_ids,
        dsad_train_ids = ["01", "04", "05", "06", "08", "09", "10", "12", "15", "16", "17", "19", "22", "23", "24", "25", "27", "28", "29", "30", "31"],
        dsad_val_ids=["03", "21", "26"],
        dsad_test_ids=["02", "07", "11", "13", "14", "18", "20", "32"],
        rd_id=0,
        rd_simple_mat=False,
        rd_simple_shape=False,
        val_transform=None,
        train_transform=None,
        image_size=None,
        predict_from_path=None,
        predict_to_path=None,
        predict_recursive=True
    ):
        super().__init__()

        self.num_workers = num_workers
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.root = root
        self.dsad_organ_ids = dsad_organ_ids
        self.dsad_train_ids=dsad_train_ids
        self.dsad_val_ids = dsad_val_ids
        self.dsad_test_ids = dsad_test_ids
        self.rd_id = rd_id
        self.rd_simple_mat = rd_simple_mat
        self.rd_simple_shape = rd_simple_shape
        self.val_transform = val_transform
        self.train_transform = train_transform
        self.image_size = image_size
        self.predict_from_path = predict_from_path
        self.predict_to_path = predict_to_path 
        self.predict_recursive = predict_recursive

    def prepare_data(self):
        """
        Lightning ensures the prepare_data() is called only within a single process on CPU, so you can safely add your downloading logic within.
        
        WARNING: prepare_data is called from the main process. It is not recommended to assign state here (e.g. self.x = y) 
        since it is called on a single process and if you assign states here then they won’t be available for other processes.
        """
        dsad = DSAD(
            self.root, 
            organ_ids=self.dsad_organ_ids,
            download=True
        )
        # print(len(dsad))
        # exit()
        RenderDataset(
            self.root,
            size=len(dsad),
            id=self.rd_id,
            image_size=self.image_size,
            generate=True,
            simple_mat=self.rd_simple_mat,
            simple_shape=self.rd_simple_shape
        )
        
    def setup(self, stage: str):
        """"
        There are also data operations you might want to perform on every GPU. Use setup() to do things like:
        - count number of classes
        - build vocabulary
        - perform train/val/test splits
        - create datasets
        - apply transforms (defined explicitly in your datamodule)
        - etc…
        """
        render_full = RenderDataset(self.root, self.rd_id, transform=self.val_transform, image_size=self.image_size)
        render_train, render_val, render_test = random_split(render_full, [0.5, 0.13, 0.37], generator=torch.Generator().manual_seed(42))

        if stage == 'fit':
            dsad_train = DSAD(
                root=self.root, 
                organ_ids=self.dsad_organ_ids, 
                selected_patient_ids=self.dsad_train_ids,
                transform=self.train_transform, 
                image_size=self.image_size
            )
            self.train_set = CombinedDataset(dsad_train, render_train)
            
            dsad_val = DSAD(
                root=self.root, 
                organ_ids=self.dsad_organ_ids, 
                selected_patient_ids=self.dsad_val_ids, 
                transform=self.val_transform, 
                image_size=self.image_size
            )
            self.val_set = CombinedDataset(dsad_val, render_val)

        if stage == 'test':
            dsad_test = DSAD(
                root=self.root, 
                organ_ids=self.dsad_organ_ids, 
                selected_patient_ids=self.dsad_test_ids, 
                transform=self.val_transform, 
                image_size=self.image_size
            )
            self.test_set = CombinedDataset(dsad_test, render_test)

        if stage == "predict":
            self.predict_set = TranslateDataset(
                from_path=self.predict_from_path,
                to_path=self.predict_to_path,
                recursive=self.predict_recursive,
                transform=self.val_transform,
                image_size=self.image_size
            )
    
    def train_dataloader(self):
        return DataLoader(self.train_set, shuffle=True, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def val_dataloader(self):
        return DataLoader(self.val_set, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def test_dataloader(self):
        return DataLoader(self.test_set, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def predict_dataloader(self):
        return DataLoader(self.predict_set, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def teardown(self, stage: str):
        # Used to clean-up when the run is finished
        pass
