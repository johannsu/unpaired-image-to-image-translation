import math
import torch
import os
from lightning import LightningDataModule
from torch.utils.data import DataLoader, random_split

from src.nn.datasets.mixed_dataset import MixedDataset
from src.nn.datasets.render import RenderDataset
from src.nn.datasets.translated_render import TranslatedRenderDataset
from src.nn.datasets.dsad import DSAD


class SegmentationDataset(LightningDataModule):
    def __init__(
        self,
        num_workers,
        batch_size,
        pin_memory,
        root, 
        organ_ids,
        train_ids=["01", "04", "05", "06", "08", "09", "10", "12", "15", "16", "17", "19", "22", "23", "24", "25", "27", "28", "29", "30", "31"],
        val_ids=["03", "21", "26"],
        test_ids=["02", "07", "11", "13", "14", "18", "20", "32"],
        val_transform=None,
        train_transform=None,
        image_size=None,
        rd_id=0,
        rd_simple_mat=False,
        rd_simple_shape=False,
        include_renders_train=False,
        include_renders_test=False,
        render_mode="raw",
        model_version="version_0",
        real_render_ratio=1/10
    ):
        super().__init__()

        self.num_workers = num_workers
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.root = root
        self.organ_ids = organ_ids
        self.train_ids = train_ids
        self.val_ids = val_ids
        self.test_ids = test_ids
        self.val_transform = val_transform
        self.train_transform = train_transform
        self.image_size = image_size
        self.rd_id = rd_id
        self.rd_simple_mat = rd_simple_mat
        self.rd_simple_shape = rd_simple_shape
        self.include_renders_train = include_renders_train
        self.render_mode = render_mode
        if self.render_mode not in ["raw", "cycle_gan", "munit", "cut"]:
            raise ValueError("include_renders_train must be one of [raw, cycle_gan, munit, cut]")
        self.model_version = model_version
        self.include_renders_test = include_renders_test
        self.real_render_ratio = real_render_ratio
        if self.real_render_ratio < 0 or self.real_render_ratio > 1:
            raise ValueError("real render ratio must be between 0 <= real_render_ratio <= 1")

    def prepare_data(self):
        """
        Lightning ensures the prepare_data() is called only within a single process on CPU, so you can safely add your downloading logic within.
        
        WARNING: prepare_data is called from the main process. It is not recommended to assign state here (e.g. self.x = y) 
        since it is called on a single process and if you assign states here then they won’t be available for other processes.
        """
        dsad_full = DSAD(self.root, organ_ids=self.organ_ids, download=True)

        # Calculate the total number of images needed to meet the real_render_ratio
        if self.real_render_ratio == 0:
            render_size = 1000
        else:
            total_images = math.ceil(len(dsad_full) / self.real_render_ratio)
        
            # Calculate the number of rendered images needed
            render_size = total_images - len(dsad_full)

        if self.include_renders_train or self.include_renders_test:
            if self.render_mode == "raw":
                RenderDataset(
                    self.root, 
                    id=self.rd_id, 
                    size=render_size,
                    transform=self.train_transform, 
                    image_size=self.image_size, 
                    generate=True,
                    simple_mat=self.rd_simple_mat,
                    simple_shape=self.rd_simple_shape
                )
            else:
                TranslatedRenderDataset(
                    self.root, 
                    id=self.rd_id,
                    model=self.render_mode,
                    checkpoint_path=os.path.join("checkpoints", self.render_mode, self.model_version, "last.ckpt"),
                    size=render_size,
                    transform=self.train_transform, 
                    image_size=self.image_size, 
                    generate=True,
                    simple_mat=self.rd_simple_mat,
                    simple_shape=self.rd_simple_shape
                )
        
    def setup(self, stage: str):
        """"
        There are also data operations you might want to perform on every GPU. Use setup() to do things like:
        - count number of classes
        - build vocabulary
        - perform train/val/test splits
        - create datasets
        - apply transforms (defined explicitly in your datamodule)
        - etc…
        """ 

        if self.include_renders_train or self.include_renders_test:
            if self.render_mode == "raw":
                render_full = RenderDataset(
                    self.root, 
                    id=self.rd_id, 
                    transform=self.val_transform, 
                    image_size=self.image_size
                )
            else:
                render_full = TranslatedRenderDataset(
                    self.root, 
                    id=self.rd_id,
                    model=self.render_mode,
                    checkpoint_path=os.path.join("checkpoints", self.render_mode, self.model_version, "last.ckpt"),
                    transform=self.train_transform, 
                    image_size=self.image_size,
                    generate=False
                )
            render_train, render_val, render_test = random_split(render_full, [0.5, 0.13, 0.37], generator=torch.Generator().manual_seed(42))

        if stage == 'fit':
            if self.real_render_ratio > 0:
                self.dsad_train = DSAD(self.root, self.organ_ids, self.train_ids, self.train_transform, self.image_size)
            if self.include_renders_train:
                if self.real_render_ratio == 0:
                    self.train_set = render_train
                else:
                    self.train_set = MixedDataset(self.dsad_train, render_train, percentage=[self.real_render_ratio, 1-self.real_render_ratio])
            else:
                self.train_set = self.dsad_train
            
            self.dsad_val = DSAD(self.root, self.organ_ids, self.val_ids, self.val_transform, self.image_size)
            if self.include_renders_test:
                self.val_set = MixedDataset(self.dsad_val, render_val, percentage=[self.real_render_ratio, 1-self.real_render_ratio])
            else:
                self.val_set = self.dsad_val

        if stage == 'test':
            self.dsad_test = DSAD(self.root, self.organ_ids, self.test_ids, self.val_transform, self.image_size)

            if self.include_renders_test:
                self.test_set = MixedDataset(self.dsad_test, render_test, percentage=[self.real_render_ratio, 1-self.real_render_ratio])
            else:
                self.test_set = self.dsad_test
    
    def train_dataloader(self):
        return DataLoader(self.train_set, shuffle=True, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def val_dataloader(self):
        return DataLoader(self.val_set, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def test_dataloader(self):
        return DataLoader(self.test_set, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory)

    def predict_dataloader(self):
        pass

    def teardown(self, stage: str):
        # Used to clean-up when the run is finished
        pass
