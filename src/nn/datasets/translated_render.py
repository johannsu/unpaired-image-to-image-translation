import os
import sys
import cv2
import torch
import numpy as np
import albumentations
from albumentations.pytorch import ToTensorV2

from tqdm import tqdm
from torchvision.utils import save_image

from src.nn.datasets.render import RenderDataset

from src.nn.cut.cut import CUT
from src.nn.munit.munit import MUNIT
from src.nn.cycle_gan.cyclegan import CycleGAN

class TranslatedRenderDataset(RenderDataset):
    # model_name = "munit"
    # model = None

    def __init__(
        self,
        root,
        id=0,
        model="munit",
        checkpoint_path="checkpoints",
        size=None,
        transform=None,
        image_size=None,
        generate=False,
        simple_mat=False,
        simple_shape=False
    ):
        self.model_name = model
        self.root = root
        self.id = id
        if size == None:
            self.size = len(list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder))))
        else:
            self.size = size
        if self.size <= 0:
            raise ValueError("Dataset size must be > 0")
        self.transform = transform
        self.image_size = image_size
        self.simple_mat = simple_mat
        self.simple_shape = simple_shape

        if model == "cycle_gan":
            self.model = CycleGAN.load_from_checkpoint(checkpoint_path)
        elif model == "munit":
            self.model = MUNIT.load_from_checkpoint(checkpoint_path)
        elif model == "cut":
            self.model = CUT.load_from_checkpoint(checkpoint_path, strict=False)
        else:
            raise ValueError("model must be one of [cycle_gan, munit, cut]")
        self.model.eval()
        
        if generate:
            self.generate()

        super(TranslatedRenderDataset, self).__init__(
            root,
            id=id,
            size=size,
            transform=transform,
            image_size=image_size,
            generate=generate,
            simple_mat=simple_mat,
            simple_shape=simple_shape
        )

    @property
    def raw_folder(self) -> str:
        return os.path.join(self.root, self.__class__.__name__, "raw", self.model_name, f"{self.id:02d}")

    def generate(self):
        if self._check_exists():
            self.img_paths = list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder)))
            return
        
        super().generate()

        trans = albumentations.Compose([
            albumentations.Normalize(normalization="min_max_per_channel", max_pixel_value=255),
            ToTensorV2()
        ])
    
        with torch.no_grad():
            for img_path in tqdm(list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder))), desc=f"Translating images using {self.model_name}"):
                # img, _ = super(TranslatedRenderDataset, self).__getitem__(i)
                path = os.path.join(self.raw_folder, img_path)
                img = cv2.imread(path)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

                img = trans(image=img)["image"]

                img = img.to(self.model.device)
                img = img.reshape(1,*img.shape)

                res = self.model(img)
                res = res[0]

                i = int(img_path[4:9])
                save_image(res, os.path.join(self.raw_folder, f"img_{(i):05d}.png"))
                
        # self.img_paths = list(filter(lambda x: "img_" in x, os.listdir(self.raw_folder)))