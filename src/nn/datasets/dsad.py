import os
from urllib.error import URLError

from torchvision.io import ImageReadMode, decode_image
from torchvision.transforms import Resize, InterpolationMode
from torch.utils.data import Dataset
import cv2

from src.nn.datasets.utils import download_and_extract_archive

class DSAD(Dataset):
    mirrors = [
        "https://springernature.figshare.com/ndownloader/files/38494425"
    ]

    resources = [
        ("DSAD.zip", "18e3514b651c572c9e421fe26cf5adbc")
    ]

    classes = [
        "abdominal_wall",
        "colon",
        "inferior_mesenteric_artery",
        "intestinal_veins",
        "liver",
        "pancreas",
        "small_intestine",
        "spleen",
        "stomach",
        "ureter",
        "vesicular_glands"
    ]

    def __init__(
            self, 
            root,
            organ_ids=None,
            selected_patient_ids=None,
            transform=None,
            image_size=None,
            download=False
    ):
        super().__init__()

        self.root = root
        self.selected_organs = None if organ_ids == None else [self.classes[i] for i in organ_ids]
        self.selected_patient_ids = selected_patient_ids
        self.transform = transform
        self.image_size = image_size

        if download:
            self.download()

        # self.patient_ids, self.organ_ids, self.images, self.masks = self.load_data()
        # dont load images into memory directly
        self.img_paths = self.load_data()

    @property
    def raw_folder(self) -> str:
        return os.path.join(self.root, self.__class__.__name__, "raw")

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, idx):
        path = self.img_paths[idx]
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        img = img[74:-74, 10:-10]

        if self.image_size is not None:
            img = cv2.resize(img, self.image_size)

        mask_file = path.replace("image", "mask")
        mask_orig = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
        
        if self.image_size is not None:
            mask_orig = cv2.resize(mask_orig,self.image_size, interpolation=cv2.INTER_NEAREST)

        mask = (mask_orig > 0)*1

        r = os.path.normpath(path)
        r = r.split(os.sep)
        organ = r[-3]

        if not self.transform is None:
            transformed = self.transform(image=img, mask=mask)

            img = transformed["image"]
            mask = transformed["mask"]
      
        return img, mask #organ
    
    def load_data(self):
        img_paths = []

        for (root, dirs, files) in os.walk(self.raw_folder):
            if os.path.isfile(os.path.join(root, "image00.png")):
                for file in files:
                    if "png" in file and file[0:5] == "image":
                        r = os.path.normpath(root)
                        r = r.split(os.sep)
                        id = r[-1]
                        organ = r[-2]

                        if (self.selected_patient_ids == None or id in self.selected_patient_ids) and \
                           (self.selected_organs == None or organ in self.selected_organs):
                            img_paths.append(os.path.join(root, file))

        return img_paths
    
    def _check_exists(self) -> bool:
        return all(
            [os.path.exists(os.path.join(self.raw_folder, p)) for p in self.classes]
        )
    
    def download(self) -> None:
        """Download the DSAD data if it doesn't exist already."""

        if self._check_exists():
            return

        os.makedirs(self.raw_folder, exist_ok=True)

        # download files
        for filename, md5 in self.resources:
            for mirror in self.mirrors:
                url = f"{mirror}"
                try:
                    print(f"Downloading {url}")
                    download_and_extract_archive(url, download_root=self.raw_folder, filename=filename, md5=md5)
                except URLError as error:
                    print(f"Failed to download (trying next):\n{error}")
                    continue
                finally:
                    print()
                break
            else:
                raise RuntimeError(f"Error downloading {filename}")
        
