# import wandb
import os
import torch
import torch.nn as nn
import torch.optim as optim
from lightning import LightningModule
from pytorch_msssim import ms_ssim
from torchmetrics.image import MultiScaleStructuralSimilarityIndexMeasure
from torchvision.models import vgg16, VGG16_Weights
# from torchmetrics.image.fid import FrechetInceptionDistance
# from torchmetrics.image.kid import KernelInceptionDistance
from prettytable import PrettyTable
from torchvision.utils import save_image

from src.nn.utils.custom_lr_schedulers import DecayAfterDelayScheduler
from src.nn.segmentation.segmentation import Segmentation
from src.nn.cycle_gan.discriminator import Discriminator
from src.nn.cycle_gan.generator import Generator
from src.nn.utils.utils import get_mosaic, set_requires_grad


class CycleGAN(LightningModule):
    def __init__(
        self, 
        lr,
        gan_w,
        cycle_w, 
        identity_w,
        vgg_w,
        ms_ssim_w,
        # seg_w,
        img_channels,
        num_residuals,
        criterion_gan,
        criterion_cycle,
        criterion_identity,
        log_img_every_n_steps,
        segmentation_model_path,
        predict_save_path
    ):
        super().__init__()
        self.save_hyperparameters()
        self.automatic_optimization = False

        self.disc_a = Discriminator(in_channels=img_channels)
        self.disc_b = Discriminator(in_channels=img_channels)
        self.gen_a2b = Generator(img_channels=img_channels, num_residuals=num_residuals)
        self.gen_b2a = Generator(img_channels=img_channels, num_residuals=num_residuals)

        self.criterion_gan = criterion_gan
        self.criterion_cycle = criterion_cycle
        self.criterion_identity = criterion_identity
        self.instancenorm = nn.InstanceNorm2d(512, affine=False)

        self.ms_ssim = MultiScaleStructuralSimilarityIndexMeasure()

        if 'vgg_w' in self.hparams.keys() and self.hparams.vgg_w > 0:
            weights = VGG16_Weights.IMAGENET1K_FEATURES
            self.vgg = vgg16(weights).features[:23].eval()

            self.vgg_preprocess = weights.transforms()
            for param in self.vgg.parameters():
                param.requires_grad = False

        try:
            self.segmentation_model = Segmentation.load_from_checkpoint(segmentation_model_path)
            self.segmentation_model.freeze()
        except:
            print("Could not load seg model")

        # self.fid = FrechetInceptionDistance()
        # self.kid = KernelInceptionDistance()

    def forward(self, real):
        return self.gen_b2a(real)

    def forward_dev(self, real_a, real_b):
        fake_a = self.gen_b2a(real_b)
        fake_b = self.gen_a2b(real_a)

        cycle_a = self.gen_b2a(fake_b)
        cycle_b = self.gen_a2b(fake_a)

        return fake_a, fake_b, cycle_a, cycle_b

    def _get_discriminator_loss(self, D_a_real, D_a_fake, D_b_real, D_b_fake):
        D_b_real_loss = self.criterion_gan(D_b_real, torch.ones_like(D_b_real))
        D_b_fake_loss = self.criterion_gan(D_b_fake, torch.zeros_like(D_b_fake))
        D_b_loss = D_b_real_loss + D_b_fake_loss
 
        D_a_real_loss = self.criterion_gan(D_a_real, torch.ones_like(D_a_real))
        D_a_fake_loss = self.criterion_gan(D_a_fake, torch.zeros_like(D_a_fake))
        D_a_loss = D_a_real_loss + D_a_fake_loss

        D_loss = (D_a_loss + D_b_loss) / 2

        return D_loss, D_a_loss, D_b_loss

    def _get_adversarial_loss(self, fake_a, fake_b):
        D_a_fake = self.disc_a(fake_a)
        D_b_fake = self.disc_b(fake_b)
        G_a2b_loss = self.criterion_gan(D_a_fake, torch.ones_like(D_a_fake))
        G_b2a_loss = self.criterion_gan(D_b_fake, torch.ones_like(D_b_fake))

        return G_a2b_loss, G_b2a_loss

    def _get_cycle_loss(self, real_a, real_b, cycle_a, cycle_b):
        C_a_loss = self.criterion_cycle(real_a, cycle_a)
        C_b_loss = self.criterion_cycle(real_b, cycle_b)

        return C_a_loss, C_b_loss

    def _get_identity_loss(self, real_a, real_b):
        if self.hparams.identity_w != 0:
            # generator for class a should do nothing if input is already in category a and vice versa
            identity_a = self.gen_b2a(real_a) 
            identity_b = self.gen_a2b(real_b)
            I_a_loss = self.criterion_identity(real_a, identity_a)
            I_b_loss = self.criterion_identity(real_b, identity_b)
        else:
            I_a_loss = I_b_loss = torch.tensor(0.0)

        return I_a_loss, I_b_loss
    
    def _get_perceptual_loss(self, img, target):
        img_vgg = self.vgg_preprocess(img)
        target_vgg = self.vgg_preprocess(target)

        img_fea = self.vgg(img_vgg)
        target_fea = self.vgg(target_vgg)

        return torch.mean((self.instancenorm(img_fea) - self.instancenorm(target_fea)) ** 2)
    
    def _get_segmentation_loss(self, img, mask):
        res = self.segmentation_model(img)
        metrics = self.segmentation_model.compute_metrics(res, mask)

        return 1 / metrics["f1_score"] if metrics["f1_score"] != 0 else 1

    def _get_generator_loss(self, real_a, real_b, fake_a, fake_b, cycle_a, cycle_b):
        # adversarial loss for both generators
        G_a2b_loss, G_b2a_loss = self._get_adversarial_loss(fake_a, fake_b) 

        # cycle loss for both generators
        C_a_loss, C_b_loss = self._get_cycle_loss(real_a, real_b, cycle_a, cycle_b)

        # Identity loss
        if self.hparams.identity_w > 0:
            I_a_loss, I_b_loss = self._get_identity_loss(real_a, real_b)
        else:
            I_a_loss = I_b_loss = torch.tensor(0, dtype=torch.float32)

        # Structural similarity:
        if self.hparams.ms_ssim_w > 0:
            # x_a_brightness = torch.mean(real_a, dim=1, keepdim=True)
            # x_b_brightness = torch.mean(real_b, dim=1, keepdim=True)
            # fake_a_brightness = torch.mean(fake_a, dim=1, keepdim=True)
            # fake_b_brightness = torch.mean(fake_b, dim=1, keepdim=True)
            loss_msssim_a = -self.ms_ssim(real_b, fake_a) # normalize=True
            loss_msssim_b = -self.ms_ssim(real_a, fake_b) # normalize=True
        else:
            loss_msssim_a = loss_msssim_b = torch.tensor(0, dtype=torch.float32)
            
        # domain-invariant perceptual loss
        loss_vgg_a = self._get_perceptual_loss(fake_a, real_a) if self.hparams.vgg_w > 0 else torch.tensor(0, dtype=torch.float32)
        loss_vgg_b = self._get_perceptual_loss(fake_b, real_b) if self.hparams.vgg_w > 0 else torch.tensor(0, dtype=torch.float32)

        # identity loss for both generators
        G_loss = G_a2b_loss * self.hparams.gan_w + \
                 G_b2a_loss * self.hparams.gan_w + \
                 C_a_loss * self.hparams.cycle_w + \
                 C_b_loss * self.hparams.cycle_w + \
                 I_a_loss * self.hparams.identity_w + \
                 I_b_loss * self.hparams.identity_w + \
                 loss_msssim_a * self.hparams.ms_ssim_w + \
                 loss_msssim_b * self.hparams.ms_ssim_w + \
                 loss_vgg_a * self.hparams.vgg_w + \
                 loss_vgg_b * self.hparams.vgg_w
        
        return G_loss, G_a2b_loss, G_b2a_loss, C_a_loss, C_b_loss, I_a_loss, I_b_loss, loss_msssim_a, loss_msssim_b, loss_vgg_a, loss_vgg_b 
    
    def shared_step(self, batch, batch_idx, split):
        real_a, _, real_b, mask_b = batch
        fake_a, fake_b, cycle_a, cycle_b = self.forward_dev(real_a, real_b)

        if split == "train":
            opt_disc, opt_gen = self.optimizers()
            set_requires_grad([self.disc_a, self.disc_b], True)
            opt_disc.zero_grad()

        D_a_real = self.disc_a(real_a)
        D_a_fake = self.disc_a(fake_a)
        D_b_real = self.disc_b(real_b)
        D_b_fake = self.disc_b(fake_b)

        # Train discriminators
        D_loss, D_a_loss, D_b_loss = self._get_discriminator_loss(D_a_real, D_a_fake, D_b_real, D_b_fake)
        
        if split == "train":
            self.manual_backward(D_loss, retain_graph=True)
            opt_disc.step()
        
        # Train Generators
        if split == "train":
            set_requires_grad([self.disc_a, self.disc_b], False)
            opt_gen.zero_grad()

        
        G_loss, G_a2b_loss, G_b2a_loss, C_a_loss, C_b_loss, I_a_loss, I_b_loss, loss_msssim_a, loss_msssim_b, loss_vgg_a, loss_vgg_b = self._get_generator_loss(real_a, real_b, fake_a, fake_b, cycle_a, cycle_b) 
        
        if split == "train":
            self.manual_backward(G_loss)
            opt_gen.step()

        # self.fid.update(real_a, True)
        # self.kid.update(real_a, True)

        # self.fid.update(fake_a, False)
        # self.kid.update(fake_a, False)

        # Logging
        output = {
            "loss": (G_loss + D_loss).detach(),
            "loss_G": G_loss.detach(),
            "loss_G_a2b": G_a2b_loss.detach(),
            "loss_G_b2a": G_b2a_loss.detach(),
            "loss_cycle_a": C_a_loss.detach(),
            "loss_cycle_b": C_b_loss.detach(),
            "loss_identity_a": I_a_loss.detach(),
            "loss_identity_b": I_b_loss.detach(),
            "loss_ms_ssim_a": loss_msssim_a.detach(),
            "loss_ms_ssim_b": loss_msssim_b.detach(),
            "loss_vgg_a": loss_vgg_a.detach(),
            "loss_vgg_b": loss_vgg_b.detach(),
            "loss_D": D_loss.detach(),
            "loss_D_a": D_a_loss.detach(), 
            "loss_D_b": D_b_loss.detach(),
            # "fid": self.fid.compute().detach(),
            # "kid_mean": self.kid.compute()[0].detach(),
            # "kid_std": self.kid.compute()[1].detach(),
        }

        if split == 'val':
            res = self.segmentation_model(fake_a)
            metrics = self.segmentation_model.compute_metrics(res, mask_b)

            output = {
                **output,
                "accuracy": metrics["accuracy"],
                "precision": metrics["precision"],
                "recall": metrics["recall"],
                "jaccard_index": metrics["jaccard_index"],
                "f1_score": metrics["f1_score"],
            }

        log_dict = {split + "/" + k: v for k,v in output.items()}
        self.log_dict(log_dict)

        if batch_idx % self.hparams.log_img_every_n_steps == 0:
            # # logging histograms
            # for name,params in self.named_parameters():
            #     self.logger.experiment.add_histogram(name,params,self.current_epoch)
            res = self.segmentation_model(fake_a)
            pred = torch.argmax(res, 1)
            
            grid_a = get_mosaic(
                real_a.cpu().numpy(),
                fake_b.detach().cpu().numpy(),
                cycle_a.detach().cpu().numpy(),
            )
            grid_b = get_mosaic(
                real_b.cpu().numpy(),
                fake_a.detach().cpu().numpy(),
                cycle_b.detach().cpu().numpy(),
                mask_b.detach().cpu().numpy(),
                pred.detach().cpu().numpy(),
            )

            self.logger.experiment.add_image('Real A, Fake B, Cycle A', grid_a, global_step=int(self.global_step))
            self.logger.experiment.add_image('Real B, Fake A, Cycle B, Mask Real B, Mask Fake A', grid_b, global_step=int(self.global_step))

        return output

    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, batch_idx, "train")

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, batch_idx, "val")
    
    def test_step(self, batch, batch_idx):
        real_a, _, real_b, mask_b = batch
        fake_a, fake_b, cycle_a, cycle_b = self.forward_dev(real_a, real_b)

        res = self.segmentation_model(fake_a)
        metrics = self.segmentation_model.compute_metrics(res, mask_b)


        if batch_idx == 0:
            self.test_accuracy = [metrics["accuracy"]]
            self.test_precision = [metrics["precision"]]
            self.test_recall = [metrics["recall"]]
            self.test_jaccard_index = [metrics["jaccard_index"]]
            self.test_f1_score = [metrics["f1_score"]]
            self.test_specificity = [metrics["specificity"]]
        else:
            self.test_accuracy.append(metrics["accuracy"])
            self.test_precision.append(metrics["precision"])
            self.test_recall.append(metrics["recall"])
            self.test_jaccard_index.append(metrics["jaccard_index"])
            self.test_f1_score.append(metrics["f1_score"])
            self.test_specificity.append(metrics["specificity"])

    def predict_step(self, batch, batch_idx):
        imgs, paths = batch
        res = self(imgs)

        for img, path in zip(res, paths):
            save_image(img, os.path.join(self.hparams.predict_save_path, path))
    
    def on_train_start(self, *args, **kwargs):
        # log network graph
        sampleImgA=torch.rand((1,3,256,256)).to(self.device)
        # sampleImgB=torch.rand((1,3,256,256)).to(self.device)

        self.logger.experiment.add_graph(self,[sampleImgA])

    def on_train_epoch_end(self, *args, **kwargs):
        scheduler_G, scheduler_D = self.lr_schedulers()
        scheduler_G.step()
        scheduler_D.step()

    def on_test_epoch_end(self):
        # Compute mean and standard deviation for each metric
        mean_accuracy = torch.mean(torch.tensor(self.test_accuracy))
        std_accuracy = torch.std(torch.tensor(self.test_accuracy))

        mean_precision = torch.mean(torch.tensor(self.test_precision))
        std_precision = torch.std(torch.tensor(self.test_precision))

        mean_recall = torch.mean(torch.tensor(self.test_recall))
        std_recall = torch.std(torch.tensor(self.test_recall))

        mean_jaccard_index = torch.mean(torch.tensor(self.test_jaccard_index))
        std_jaccard_index = torch.std(torch.tensor(self.test_jaccard_index))

        mean_f1_score = torch.mean(torch.tensor(self.test_f1_score))
        std_f1_score = torch.std(torch.tensor(self.test_f1_score))

        mean_specificity = torch.mean(torch.tensor(self.test_specificity))
        std_specificity = torch.std(torch.tensor(self.test_specificity))

        # Create a PrettyTable object
        table = PrettyTable()

        # Set column names
        table.field_names = ["Metric", "Value"]

        # Align columns: left, right
        table.align["Metric"] = "l"
        table.align["Value"] = "r"

        # Vertical alignment: middle
        table.valign["Metric"] = "m"
        table.valign["Value"] = "m"

        table.add_row(["Accuracy", f'{mean_accuracy:.2f} ± {std_accuracy:.2f}'])
        table.add_row(["Precision", f'{mean_precision:.2f} ± {std_precision:.2f}'])
        table.add_row(["Recall", f'{mean_recall:.2f} ± {std_recall:.2f}'])
        table.add_row(["Jaccard Index", f'{mean_jaccard_index:.2f} ± {std_jaccard_index:.2f}'])
        table.add_row(["F1 Score", f'{mean_f1_score:.2f} ± {std_f1_score:.2f}'])
        table.add_row(["Specificity", f'{mean_specificity:.2f} ± {std_specificity:.2f}'])

        # Print the table
        print(table)

    def configure_optimizers(self):
        opt_disc = optim.Adam(
            list(self.disc_a.parameters()) + list(self.disc_b.parameters()),
            lr=self.hparams.lr,
            betas=(0.5, 0.999)
        )
        opt_gen = optim.Adam(
            list(self.gen_a2b.parameters()) + list(self.gen_b2a.parameters()),
            lr=self.hparams.lr,
            betas=(0.5, 0.999)
        )
        
        # Does nothing - placeholder
        # scheduler_disc = torch.optim.lr_scheduler.LambdaLR(opt_disc, lr_lambda=lambda epoch: 1.0)
        # scheduler_gen = torch.optim.lr_scheduler.LambdaLR(opt_gen, lr_lambda=lambda epoch: 1.0)
        total_epochs = 200
        scheduler_disc = DecayAfterDelayScheduler(opt_disc, total_epochs)
        scheduler_gen = DecayAfterDelayScheduler(opt_disc, total_epochs)

        return [opt_disc, opt_gen], [scheduler_disc, scheduler_gen]
        # return {'optimizer': opt_disc, 'frequency': 5}, {'optimizer': opt_gen, 'frequency': 1}

    def load_state_dict(self, state_dict, strict=False, assign=False):
        return super().load_state_dict(state_dict, strict=False, assign=assign)