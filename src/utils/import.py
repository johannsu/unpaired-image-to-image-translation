import os
import shutil

dest_path = "/Users/johann/github/unpaired-image-to-image-translation/datasets/DSAD/"

def resolve(path):
    if path.endswith(".png") and "image" in path:
        last_idx = max((int(s[4:9]) for s in os.listdir(dest_path)), default=-1)
        shutil.copyfile(path, os.path.join(dest_path, f"img_{last_idx+1:05d}.png"))
        return
    
    try:
        for p in os.listdir(path):
            resolve(os.path.join(path, p))
    except NotADirectoryError:
        # print(f"Not a directory: {path}")
        return

def main():
    resolve("/Users/johann/Downloads/DSAD/")

if __name__ == '__main__':
    main()