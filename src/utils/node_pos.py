import bpy
import os
import sys

# sys.path.append("/Users/johann/anaconda3/envs/bachelor-thesis/lib/python3.11/site-packages")
sys.path.append("C:\\Users\\Johan\\AppData\\Roaming\\Python\\Python311\\site-packages")

import yaml

# OUT_PATH = os.path.join(os.sep, "Users", "johann", "Developer", "github", "unpaired-image-to-image-translation", "src", "scene", "nodes")
# OUT_PATH = os.path.join(os.sep, "wsl.localhost", "Ubuntu", "home", "johann", "code", "unpaired-image-to-image-translation", "src", "scene", "nodes")
OUT_PATH = "C:\\Users\\Johan"

bl_info = {
    "name": "Node Dev",
    "author": "Johann",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "NODE EDITOR > Node > Node Dev Util",
    "description": "Shows node info for python",
    "warning": "",
    "doc_url": "",
    "category": "Node",
}

class SaveYml(bpy.types.Operator):
    bl_idname = "wm.save_yml"
    bl_label = "Save yaml file"

    def execute(self, context):
        node_tree = context.space_data.edit_tree

        interface = list()
        for inp in [s for s in node_tree.interface.items_tree if s.item_type == "SOCKET"]:
            s = dict()
            s['index'] = inp.index
            s['position'] = inp.position
            s['attribute_domain'] = inp.attribute_domain
            s['bl_socket_idname'] = inp.bl_socket_idname
            s['name'] = inp.name
            s['in_out'] = inp.in_out
            s['identifier'] = inp.identifier

            interface.append(s)

        nodes = list()

        for node in node_tree.nodes:
            n = dict()
            n['name'] = node.name
            n['type'] = node.bl_idname
            n['location'] = [node.location[0], node.location[1]]

            match node.bl_idname:
                case "ShaderNodeValToRGB":
                    n['color_ramp'] = dict()
                    n['color_ramp']['color_mode'] = node.color_ramp.color_mode
                    n['color_ramp']['interpolation'] = node.color_ramp.interpolation
                    
                    n['color_ramp']['elements'] = list()
                    for elem in node.color_ramp.elements:
                        e = dict()
                        e['alpha'] = elem.alpha
                        e['color'] = [elem.color[0],elem.color[1],elem.color[2],elem.color[3]]
                        e['position'] = elem.position
                        n['color_ramp']['elements'].append(e)

                case "ShaderNodeFloatCurve": 
                    n['mapping'] = dict()
                    n['mapping']['curves'] = list()
                    for curve in node.mapping.curves:
                        c = dict()
                        c['points'] = list()
                        for point in curve.points:
                            p = dict()
                            p['location'] = [point.location.x, point.location.y]
                            p['handle_type'] = point.handle_type
                            c['points'].append(p)
                        n['mapping']['curves'].append(c)

                case t:
                    # keys = [k for k in node.keys()] #  if k not in ["name", "type", "location", "inputs", "connection"]
                    default_keys = ["__doc__", "__module__", "__slots__", "__slotnames__", "is_active_output", "bl_rna", "draw_buttons", "draw_buttons_ext", "input_template", "is_registered_node_type", "output_template", "poll", "poll_instance", "rna_type", "update", "bl_description", "bl_height_default", "bl_height_max", "bl_height_min", "bl_icon", "bl_idname", "bl_label", "bl_static_type", "bl_width_default", "bl_width_max", "bl_width_min", "color", "dimensions", "height", "hide", "inputs", "internal_links", "label", "location", "name", "outputs", "parent", "select", "show_options", "show_preview", "show_texture", "type", "use_custom_color", "width", "socket_value_update"]
                    keys = [k for k in dir(node) if k not in default_keys]
                    print(f"{t} - {keys}")
                    if len(keys) > 0:
                        for k in keys:
                            val = getattr(node, k)
                            if type(val) in [str, float, int, bool, set, list, tuple]:
                                # n[k] = node[k]
                                n[k] = val
                            else:
                                print(f"non primitive data type: {t}.{k}")

            n['inputs'] = list()

            for inp in node.inputs:
                i = dict()
                i['identifier'] = inp.identifier
                i['type'] = inp.type
                i['name'] = inp.name

                if inp.is_linked:
                    if inp.is_multi_input:
                        i["multi_connection"] = list()
                        for link in inp.links:
                            from_node = link.from_node
                            from_socket = link.from_socket

                            if from_node.bl_idname == "NodeGroupInput":
                                i['multi_connection'].append(dict(node=from_node.name, name=from_socket.name))
                            else:
                                i['multi_connection'].append(dict(node=from_node.name, socket_id=from_socket.identifier))
                    else:
                        from_node = inp.links[0].from_node
                        from_socket = inp.links[0].from_socket

                        if from_node.bl_idname == "NodeGroupInput":
                            i['connection'] = dict(node=from_node.name, name=from_socket.name)
                        else:
                            i['connection'] = dict(node=from_node.name, socket_id=from_socket.identifier)
                else:
                    match inp.type:
                        case "VECTOR": i['default_value'] = [
                            inp.default_value[0], 
                            inp.default_value[1], 
                            inp.default_value[2]]
                        case "ROTATION": 
                            i['default_value'] = dict(
                                rotation=[inp.default_value.x,inp.default_value.y,inp.default_value.z],
                                direction=inp.default_value.order
                            )
                        case "RGBA": i['default_value'] = [
                            inp.default_value[0], 
                            inp.default_value[1], 
                            inp.default_value[2],
                            inp.default_value[3]]
                        case other:
                            if other in ["VALUE", "INT", "BOOLEAN", "STRING"]: 
                                i['default_value'] = inp.default_value

                n['inputs'].append(i)

            nodes.append(n)

        data = dict(
            node_tree = dict(
                interface = interface,
                nodes = nodes,
            )
        )

        if node_tree.type == "GEOMETRY":
            name = node_tree.name.lower().replace(" ", "_")
        else:
            name = context.space_data.id.name.lower().replace(" ", "_")
        with open(os.path.join(OUT_PATH, f"{name}.yml"), 'w') as outfile:
            yaml.dump(data, outfile, default_flow_style=False)

        return {'FINISHED'}

class GEONODE_PT_show_pos(bpy.types.Panel):
    bl_space_type = "NODE_EDITOR"
    bl_region_type = "UI"
    bl_label = "Node Dev Util"
    bl_category = "Node"
   
    def draw(self, context):
        if len(context.selected_nodes) > 0:
            n = context.selected_nodes[0]

            row_1 = self.layout.row()
            row_1.label(text=f'Name: {n.name}')

            row_2 = self.layout.row()
            row_2.label(text=f'Type: {n.bl_idname}')

            row_3 = self.layout.row()
            row_3.label(text=f'Location: ({n.location.x}, {n.location.y})')

        row_2 = self.layout.row()
        row_2.operator("wm.save_yml", text='Export as yaml', icon='COPYDOWN')
        
def register():
    bpy.utils.register_class(GEONODE_PT_show_pos)
    bpy.utils.register_class(SaveYml)

def unregister():
    bpy.utils.unregister_class(GEONODE_PT_show_pos)
    bpy.utils.unregister_class(SaveYml)

if __name__ == '__main__':
    register()