import yaml
from src.utils import node_pos
from mathutils import Vector, Euler, Color

def get_node_by_name(node_tree, name):
    n = node_tree.nodes.get(name)
    return n

def get_input_socket_by_identifier(node, socket_id):
    try:
        return next(s for s in node.inputs if s.identifier == socket_id)
    except StopIteration:
        s = str([n.identifier for n in node.inputs])
        print(f"No input socket on node '{node.name}' found for id: '{socket_id}', only {s}")
        return None
    
def get_input_socket_by_name(node, name):
    try:
        return next(s for s in node.inputs if s.bl_label == name)
    except StopIteration:
        s = str([n.name for n in node.inputs])
        print(f"No input socket on node '{node.name}' found for name: '{name}', only {s}")
        return None

def get_output_socket_by_identifier(node, socket_id):
    try:
        return next(s for s in node.outputs if s.identifier == socket_id)
    except StopIteration:
        s = str([n.identifier for n in node.outputs])
        print(f"No output socket on node '{node.name}' found for id: '{socket_id}', only {s}")

def get_output_socket_by_name(node, name):
    try:
        return next(s for s in node.outputs if s.name == name)
    except StopIteration:
        s = str([n.name for n in node.outputs])
        print(f"No output socket on node '{node.name}' found for name: '{name}', only {s}")
        return None

def add_node(node_tree, node):
    n = get_node_by_name(node_tree, node['name'])
    if not n:
        n = node_tree.nodes.new(type=node['type'])
        n.name = node['name']
    
    n.location = node['location']

    match node['type']:
        case "ShaderNodeValToRGB":
            n.color_ramp.color_mode = node['color_ramp']['color_mode']
            n.color_ramp.interpolation = node['color_ramp']['interpolation']

            if len(node['color_ramp']['elements']) > 2:
                for _ in range(len(node['color_ramp']['elements']) - 2):
                   n.color_ramp.elements.new(0)
            for idx, el in enumerate(node['color_ramp']['elements']):
                n.color_ramp.elements[idx].alpha = el['alpha']
                n.color_ramp.elements[idx].color = (el['color'][0], el['color'][1], el['color'][2], el['color'][3])
                n.color_ramp.elements[idx].position = el['position']

        case "ShaderNodeFloatCurve":
            if len(node['mapping']['curves'][0]['points']) > 2:
                for _ in range(len(node['mapping']['curves'][0]['points']) - 2):
                    n.mapping.curves[0].points.new(position=0, value=0)
            for idx, p in enumerate(node['mapping']['curves'][0]['points']):
                n.mapping.curves[0].points[idx].location = (p['location'][0], p['location'][1])
                n.mapping.curves[0].points[idx].handle_type = p['handle_type']

        case t:
            keys = [k for k in node.keys() if k not in ["name", "type", "location", "inputs"]]
            if len(keys) > 0:
                for k in keys:
                    setattr(n, k, node[k])

    for inp in node['inputs']:
        if "default_value" in inp.keys():
            socket = get_input_socket_by_identifier(n, inp['identifier'])
            match inp['type']:
                # case "VECTOR": n.inputs[inp['id']].default_value = Vector(inp['default_value'])
                case "VECTOR": 
                    socket.default_value = Vector(inp['default_value'])
                case "ROTATION": 
                    socket.default_value = Euler(inp['default_value']['rotation'], inp['default_value']['direction'])
                case "RGBA":
                    socket.default_value = inp['default_value']
                case _: 
                    socket.default_value = inp['default_value']

def add_connection(node_tree, node):
    n = get_node_by_name(node_tree, node['name'])

    for inp in node['inputs']:
        if "connection" in inp.keys():
            opposite_node = get_node_by_name(node_tree, inp['connection']['node'])

            if n.bl_idname == "NodeGroupOutput":
                input_socket = get_input_socket_by_name(n, inp['name'])
            else:
                input_socket = get_input_socket_by_identifier(n, inp['identifier'])

            if opposite_node.bl_idname == "NodeGroupInput":
                output_socket = get_output_socket_by_name(opposite_node, inp['connection']['name']) 
            else:
                output_socket = get_output_socket_by_identifier(opposite_node, inp['connection']['socket_id'])

            node_tree.links.new(output_socket, input_socket)
        elif "multi_connection" in inp.keys():
            for connection in inp['multi_connection']:
                opposite_node = get_node_by_name(node_tree, connection['node'])

                if n.bl_idname == "NodeGroupOutput":
                    input_socket = get_input_socket_by_name(n, inp['name'])
                else:
                    input_socket = get_input_socket_by_identifier(n, inp['identifier'])

                if opposite_node.bl_idname == "NodeGroupInput":
                    output_socket = get_output_socket_by_name(opposite_node, connection['name']) 
                else:
                    output_socket = get_output_socket_by_identifier(opposite_node, connection['socket_id'])

                node_tree.links.new(output_socket, input_socket)

def add_interface_socket(node_tree, socket):
    i = node_tree.interface.new_socket(socket['name'], description='', in_out=socket['in_out'], socket_type=socket['bl_socket_idname'])
    i.attribute_domain = socket['attribute_domain']

def load_node_tree(node_tree, path):
    with open(path, 'r') as file:
        nodes = yaml.safe_load(file)

    for soc in nodes['node_tree']['interface']:
        add_interface_socket(node_tree, soc)

    for node in nodes['node_tree']['nodes']:
        add_node(node_tree, node=node)
    
    for node in nodes['node_tree']['nodes']:
        add_connection(node_tree, node)