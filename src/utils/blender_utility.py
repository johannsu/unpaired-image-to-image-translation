import bpy
import os
from .load_node_tree import load_node_tree

def find_name(name: str, collection) -> str:
    if not name[-3:].isnumeric():
        name = f"{name}.{0:03d}"

    if name in collection:
        name = name[:-3]
        i = 0
        while f"{name}{i:03d}" in collection:
            i+=1
        name = f"{name}{i:03d}"

    return name

def load_geo_nodes_modifier(mod_name: str, path: str):
    if mod_name not in bpy.data.node_groups.keys():
        # GeometryNodeTree
        geo_nodes = bpy.data.node_groups.new(mod_name, type="GeometryNodeTree")
        # geo_nodes.interface.new_socket("Geometry", in_out='INPUT', socket_type='NodeSocketGeometry')
        # geo_nodes.interface.new_socket("Geometry", in_out='OUTPUT', socket_type='NodeSocketGeometry')

        load_node_tree(geo_nodes, path)
    else:
        geo_nodes = bpy.data.node_groups.get(mod_name)

    return geo_nodes

def load_material(mat_name: str, path: str):
    if mat_name not in bpy.data.materials.keys():
        bowel_mat = bpy.data.materials.new(mat_name)
        bowel_mat.use_nodes = True

        node_tree = bowel_mat.node_tree

        load_node_tree(node_tree, path)
    else:
        bowel_mat = bpy.data.materials.get(mat_name)
    
    return bowel_mat