import numpy as np
from scipy.spatial import cKDTree

def voronoi(width: int, height: int, density:int, seed:int=None) -> list[float]:
    np.random.seed(seed)
    
    points = [[np.random.randint(0, height), np.random.randint(0, width)] for _ in range(density)]  # Generates Points(y, x)
    coord = np.dstack(np.mgrid[0:height, 0:width]) # Makes array with coordinates as values

    tree = cKDTree(points)  # Build Tree
    distances = tree.query(coord)[0]  # Calculate distances
    distances *= 1.0/distances.max()

    return distances