import bpy
import os
import shutil
from os import path
from bpy import context
from bpy import ops
import csv
import bmesh
from math import *
from mathutils import *
import numpy as np
from numpy import genfromtxt

BLENDER_PATH="/Users/johann/github/unpaired-image-to-image-translation/blender"

# copy file
shutil.copy(f"{BLENDER_PATH}/init.blend", f"{BLENDER_PATH}/blend_files/intermediate.blend")

# render settings
scene = bpy.context.scene
scene.cycles.device = 'GPU'
"""
prefs = bpy.context.user_preferences
cprefs = prefs.addons['cycles'].preferences

# Attempt to set GPU device types if available
for compute_device_type in ('CUDA', 'OPENCL', 'NONE'):
    try:
        cprefs.compute_device_type = compute_device_type
        break
    except TypeError:
        pass
"""

# transition----------------------------------------------------------------------
try:
    bpy.ops.object.mode_set(mode='OBJECT')
except:
    pass

bpy.ops.object.select_all(action='DESELECT')

# background----------------------------------------------------------------------

# create bottom plate & background collection
background = bpy.data.collections.new('background')
bpy.context.scene.collection.children.link(background)

bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, -1))
btmp = bpy.context.object
me = btmp.data
btmp.name = 'BTMP'

bpy.ops.transform.resize(value=(1, 1, 0.05), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(btmp)


# create right side plate
bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 1, 0))
rsdp = bpy.context.object
me = rsdp.data
rsdp.name = 'RSDP'

bpy.ops.transform.resize(value=(1, 0.05, 1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=0.1, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.transform.translate(value=(0, -0.3, 0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.object.editmode_toggle()

bpy.ops.transform.rotate(value=0.18, orient_axis='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.object.mode_set(mode='OBJECT')

bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(rsdp)

# create back plate
bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(-1, 0, 0))
bakp = bpy.context.object
me = bakp.data
bakp.name = 'BAKP'

bpy.ops.transform.resize(value=(0.05, 1, 1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(bakp)

# join plates into one object, renaming to plates
context.view_layer.objects.active = bakp
bakp.select_set(True)
bpy.ops.object.select_all(action='DESELECT')
bpy.data.objects['BTMP'].select_set(True)
bpy.data.objects['BAKP'].select_set(True)
bpy.data.objects['RSDP'].select_set(True)
bpy.ops.object.join()
bpy.data.objects['BAKP'].name = "plates"

bpy.ops.object.select_all(action='DESELECT')
bpy.data.objects['plates'].select_set(True)

bpy.ops.transform.resize(value=(0.5, 0.5, 0.5), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.transform.translate(value=(0.8, 0.05, 0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.transform.translate(value=(0, -0.15, 0.25), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
    
bpy.ops.transform.translate(value=(-0.15, 0, 0.20), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

# put on wall coloring
wall = bpy.data.materials.new("wall")
wall.use_nodes = True
bkg = wall.node_tree
nodes = bkg.nodes
bsdf = nodes["Principled BSDF"]
bsdf.inputs["Base Color"].default_value = (0.8, 0.323, 0.428, 1)
wall.diffuse_color = (0.8, 0.323, 0.428, 1)
bpy.context.view_layer.objects.active = bakp
bakp.select_set(True)
bakp.active_material = wall

# create organ
bpy.ops.object.metaball_add(type='ELLIPSOID', radius=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), rotation=(0, 0, 0))
organ = bpy.context.object
me = organ.data
organ.name = 'organ'

bpy.ops.transform.resize(value=(1, 0.5, 0.3), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.transform.rotate(value=-1.3, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(organ)

bpy.ops.object.select_all(action='DESELECT')
bpy.data.objects['organ'].select_set(True)

bpy.ops.transform.resize(value=(0.2, 0.2, 0.2), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.transform.translate(value=(-0.12, 0, -0.5), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, release_confirm=True)

bpy.ops.transform.translate(value=(-0.2, 0, 0.41), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

# put on organ coloring
org = bpy.data.materials.new("org")
org.use_nodes = True
bkgo = org.node_tree
nodes = bkgo.nodes
bsdf = nodes["Principled BSDF"]
bsdf.inputs["Base Color"].default_value = (0.8, 0.00643981, 0.0335402, 1)
org.diffuse_color = (0.8, 0.00643981, 0.0335402, 1)
bpy.context.view_layer.objects.active = organ
organ.select_set(True)
organ.active_material = org

# create vessel
bpy.ops.mesh.primitive_cylinder_add(radius=0.01, depth=0.5, enter_editmode=False, align='WORLD', location=(0, 0, 0))
vessel = bpy.context.object
me = vessel.data
vessel.name = 'vessel'

bpy.ops.transform.translate(value=(0, -0.075, -0.03), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=1.58825, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=0.549779, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(vessel)

# put on vessel coloring
ves = bpy.data.materials.new("ves")
ves.use_nodes = True
veso = ves.node_tree
nodes = veso.nodes
bsdf = nodes["Principled BSDF"]
bsdf.inputs["Base Color"].default_value = (0.8, 0.00643981, 0.0335402, 1)
ves.diffuse_color = (0.8, 0.00643981, 0.0335402, 1)
bpy.context.view_layer.objects.active = vessel
vessel.select_set(True)
vessel.active_material = ves

# create fat
bpy.ops.object.metaball_add(type='ELLIPSOID', radius=0.05, enter_editmode=False, align='WORLD', location=(-0.193, 0.0729, -0.04))
fat1 = bpy.context.object
me = fat1.data
fat1.name = 'fat_a'
bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(fat1)

bpy.ops.object.metaball_add(type='ELLIPSOID', radius=0.05, enter_editmode=False, location=(-0.0309, 0.1215, -0.05))
fat2 = bpy.context.object
me = fat2.data
fat2.name = 'fat_b'
bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(fat2)

bpy.ops.object.metaball_add(type='ELLIPSOID', radius=0.03, enter_editmode=False, location=(0.0685, 0.123, -0.03))
fat3 = bpy.context.object
me = fat3.data
fat3.name = 'fat_c'
bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(fat3)

bpy.ops.object.metaball_add(type='ELLIPSOID', radius=0.04, enter_editmode=False, location=(-0.253, 0, 0))
fat4 = bpy.context.object
me = fat4.data
fat4.name = 'fat_d'
bpy.ops.collection.objects_remove_all()
bpy.data.collections['background'].objects.link(fat4)

bpy.ops.transform.rotate(value=1.244, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.resize(value=(0.993147, 1, 1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

# put on fat coloring
fat = bpy.data.materials.new("fat")
fat.use_nodes = True
bkgf = fat.node_tree
nodes = bkgf.nodes
bsdfa = nodes["Principled BSDF"]
bsdfa.inputs["Base Color"].default_value = (1, 0.43, 0, 1)
fat.diffuse_color = (1, 0.43, 0, 1)

bpy.context.view_layer.objects.active = fat1
fat1.select_set(True)
fat1.active_material = fat

bpy.context.view_layer.objects.active = fat2
fat2.select_set(True)
fat2.active_material = fat

bpy.context.view_layer.objects.active = fat3
fat3.select_set(True)
fat3.active_material = fat

bpy.context.view_layer.objects.active = fat4
fat4.select_set(True)
fat4.active_material = fat

# transition----------------------------------------------------------------------
try:
    bpy.ops.object.mode_set(mode='OBJECT')
except:
    pass

bpy.ops.object.select_all(action='DESELECT')

# make two ends of bowel----------------------------------------------------------
bpy.ops.curve.primitive_nurbs_path_add(radius=1, enter_editmode=False, location=(0, 0, 0))
bpy.ops.transform.resize(value=(0.03, 1, 1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.ops.transform.translate(value=(0.16, 0, 0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

path_1 = bpy.context.object
path_1.name = 'path_1'

bpy.ops.object.mode_set(mode = 'EDIT')
for points in bpy.context.object.data.splines.active.points:
    points.select = False

bpy.context.object.data.splines.active.points[2].select = True
bpy.ops.transform.translate(value=(0, 0, -0.02), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.context.object.data.splines.active.points[2].select = False
bpy.context.object.data.splines.active.points[3].select = True
bpy.ops.transform.translate(value=(0, -0.01, -0.03), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.context.object.data.splines.active.points[3].select = False
bpy.context.object.data.splines.active.points[4].select = True
bpy.ops.transform.translate(value=(0, -0.025, -0.035), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.context.object.data.splines.active.points[4].select = False

bpy.ops.object.mode_set(mode = 'OBJECT')
bpy.ops.curve.primitive_bezier_circle_add(radius=0.015, enter_editmode=False, location=(0, 0, 0))

circle = bpy.context.object
circle.name = 'circle'

bpy.context.view_layer.objects.active = path_1
bpy.context.object.data.bevel_object = bpy.data.objects["circle"]
bpy.data.objects['path_1'].select_set(True)
bpy.ops.object.convert(target='MESH')

bpy.ops.object.select_all(action='DESELECT')
bpy.context.view_layer.objects.active = circle
bpy.data.objects['circle'].select_set(True)
bpy.ops.object.delete()

bpy.ops.curve.primitive_nurbs_path_add(radius=1, enter_editmode=False, location=(0, 0, 0))
bpy.ops.transform.resize(value=(0.03, 1, 1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.ops.transform.translate(value=(-0.16, 0, 0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

path_2 = bpy.context.object
path_2.name = 'path_2'

bpy.ops.object.mode_set(mode = 'EDIT')
for points in bpy.context.object.data.splines.active.points:
    points.select = False

bpy.context.object.data.splines.active.points[2].select = True
bpy.ops.transform.translate(value=(0, 0, -0.03), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.context.object.data.splines.active.points[2].select = False
bpy.context.object.data.splines.active.points[3].select = True
bpy.ops.transform.translate(value=(0, -0.01, -0.04), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.context.object.data.splines.active.points[3].select = False
bpy.context.object.data.splines.active.points[4].select = True
bpy.ops.transform.translate(value=(0, -0.05, -0.045), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.context.object.data.splines.active.points[4].select = False

bpy.ops.object.mode_set(mode = 'OBJECT')
bpy.ops.transform.rotate(value=3.14159, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
bpy.ops.curve.primitive_bezier_circle_add(radius=0.015, enter_editmode=False, location=(0, 0, 0))

circle_2 = bpy.context.object
circle_2.name = 'circle_2'

bpy.context.view_layer.objects.active = path_2
bpy.context.object.data.bevel_object = bpy.data.objects["circle_2"]
bpy.data.objects['path_2'].select_set(True)
bpy.ops.object.convert(target='MESH')

bpy.ops.object.select_all(action='DESELECT')
bpy.context.view_layer.objects.active = circle_2
bpy.data.objects['circle_2'].select_set(True)
bpy.ops.object.delete()

# put on bowel coloring
bowel_cl = bpy.data.materials.new("bowel")
bowel_cl.use_nodes = True
bwln = bowel_cl.node_tree
node = bwln.nodes
bsdf = node["Principled BSDF"]
bsdf.inputs["Base Color"].default_value = (0.186, 0, 0, 1)
bowel_cl.diffuse_color = (0.186, 0, 0, 1)

bpy.context.view_layer.objects.active = path_1
path_1.select_set(True)
path_1.active_material = bowel_cl

bpy.context.view_layer.objects.active = path_2
path_2.select_set(True)
path_2.active_material = bowel_cl

# transition----------------------------------------------------------------------
try:
    bpy.ops.object.mode_set(mode='OBJECT')
except:
    pass

bpy.ops.object.select_all(action='DESELECT')

# camera--------------------------------------------------------------------------

bpy.data.objects['Camera'].select_set(True)
    
bpy.ops.transform.translate(value=(-7, 6.5, -4.6), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=0.0698132, orient_axis='Z', orient_type='VIEW', orient_matrix=((0.159881, -0.987136, -4.20958e-07), (0.388874, 0.0629841, -0.919136), (-0.907312, -0.146952, -0.393941)), orient_matrix_type='VIEW', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

# transition----------------------------------------------------------------------
try:
    bpy.ops.object.mode_set(mode='OBJECT')
except:
    pass

bpy.ops.object.select_all(action='DESELECT')

# light---------------------------------------------------------------------------

bpy.data.objects['Light'].select_set(True)
bpy.ops.transform.translate(value=(-3.7, -1.5, -5.5), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.data.lights["Light"].energy = 300
bpy.data.lights["Light"].type = 'SPOT'
bpy.data.lights["Light"].spot_size = 0.5

bpy.ops.transform.rotate(value=-68/180*3.1415, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=17.6/180*3.1415, orient_axis='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=10.8/180*3.1415, orient_axis='Z', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

bpy.ops.transform.rotate(value=3.56/180*3.1415, orient_axis='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)

# save file
print("intermediate step completed!")
bpy.ops.wm.save_as_mainfile(copy=True, filepath=f"{BLENDER_PATH}/blend_files/intermediate.blend")
