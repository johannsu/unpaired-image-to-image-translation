import os
import sys
import math
import bpy
import cv2
import numpy as np
from tqdm import tqdm
from src.utils.blender_utility import load_geo_nodes_modifier, load_material
from src.utils.load_node_tree import load_node_tree
import bmesh
from mathutils import Vector, Euler

class BowelScene:
    seed: int
    blend_file_name: str
    save_blend_file: bool
    primitive_shape: str # PLANE, CYLINDER, CONE
    
    # TODO: Add mechanism to create scene in different levels of detail and with specific features
    def __init__(
            self, 
            seed: int = None, 
            blend_file_name: str = "out.blend",
            save_blend_file: bool = False,
            simple_mat: bool = False,
            simple_shape: bool = False
        ):
        self.blend_file_name = blend_file_name
        self.seed = np.random.randint(0,1_000_000) if seed == None else seed
        self.save_blend_file = save_blend_file
        self.simple_mat = simple_mat
        self.simple_shape = simple_shape

        np.random.seed(self.seed)

        # Create an empty scene
        bpy.ops.wm.read_homefile(use_empty=True)
        self._setup_scene()

        self._build()

    def _setup_scene(self):
        scene = bpy.context.scene

        scene.frame_start = 0
        scene.frame_end = 100
        scene.frame_current = 0

        render = scene.render
        render.engine = "CYCLES"
        render.threads_mode = "AUTO"
        render.use_compositing = True
        render.use_persistent_data = True # Keep render data around for faster re-renders and animation renders, at the cost of increased memory usage
        render.film_transparent = True

        render.image_settings.color_depth = "8"
        render.image_settings.color_mode = "RGB"
        render.image_settings.compression = 15
        render.image_settings.quality = 90

        # Metadata included in render
        render.use_stamp_date = True 
        render.use_stamp_time = True
        render.use_stamp_render_time = False
        render.use_stamp_camera = True
        render.use_stamp_lens = True
        render.use_stamp_scene = True

        cycles = scene.cycles

        cycles.denoising_store_passes = True

        cycles.samples = 10

        cycles.use_guiding = True
        cycles.guiding_training_samples = 128

        cycles.use_denoising = True
        cycles.use_preview_denoising = True

        # Set raytracing bounces
        cycles.max_bounces = 12
        cycles.diffuse_bounces = 4
        cycles.glossy_bounces = 4
        cycles.transmission_bounces = 12
        cycles.volume_bounces = 0
        cycles.transparent_max_bounces = 8

        scene.use_nodes = True
        node_tree = scene.node_tree

        load_node_tree(node_tree, os.path.join(os.getcwd(), "src", "scene", "nodes", "scene.yml"))

    def _add_camera(self, location: tuple[float, float, float] = (0,0,0), rotation: tuple[float, float, float] = (0,0,0)):
        # Add camera
        bpy.ops.object.camera_add(align="WORLD", location=location, rotation=(0,0,0), scale=(1,1,1))

        cam_obj = bpy.context.object
        bpy.context.scene.camera = cam_obj
        cam_obj.rotation_euler = Euler(rotation)

        cam = cam_obj.data

        cam.type = "PERSP" # PERSP, PANO
        cam.lens = 50.0 # Camera focal length (mm)
        # cam.angle = 100.0 # Camera lens field of view

        # cam.dof.use_dof = False # use depth of field
        # cam.dof.aperture_fstop = 10.0


    def _add_light_source(self, location: tuple[float, float, float] = (0,0,0)):
        # Add light
        bpy.ops.object.light_add(type='POINT', align='WORLD', location=location)
        light = bpy.context.object.data

        light.use_nodes = True

        node_tree = light.node_tree
        load_node_tree(node_tree, os.path.join(os.getcwd(), "src", "scene", "nodes", "point.yml"))

        # light.color = mathutils.Color(1.0, 1.0, 1.0)
        light.energy = 10.0
        light.use_soft_falloff = True
        light.shadow_soft_size = 0.7

        light.cycles.cast_shadow = True
        light.cycles.use_multiple_importance_sampling = True
        light.cycles.is_caustics_light = True

    def _add_bowel_stl(self, location: tuple[float, float, float] = (0,0,0), rotation: tuple[float, float, float] = (0,0,0), path:str = None):
        if path == None:
            path = os.path.join(os.getcwd(), "blender", "assets", "Bowel_mst_5_org.stl")

        bpy.ops.import_mesh.stl(filepath=path, global_scale=3)
        bpy.ops.object.shade_smooth_by_angle()
        bowel = bpy.context.object

        # bpy.ops.object.select_camera(extend=False)
        # cam = bpy.context.object
        # cam.data.dof.focus_object = bowel
        # cam.constraints.new(type='TRACK_TO')
        # cam.constraints["Track To"].target = bowel
        # cam.constraints["Track To"].track_axis = "TRACK_NEGATIVE_Z"
        # cam.constraints["Track To"].up_axis = "UP_Y"
        # cam.constraints["Track To"].use_target_z = False

        bowel.data.materials.append(load_material("bowel_mat", os.path.join(os.getcwd(), "src", "scene", "nodes", "bowel_mat.yml")))

        bowel.location = Vector(location)
        bowel.rotation_euler = Euler(rotation)

    def _add_grasper(self, location: tuple[float, float, float] = (0,0,0), rotation: tuple[float, float, float] = (0,0,0), path:str|None = None):
        if path == None:
            path = os.path.join(os.getcwd(), "blender", "assets", "needle_holder.fbx")

        bpy.ops.import_scene.fbx(filepath=path, global_scale=3)

        grasper = bpy.context.object

        grasper.location = Vector(location)
        grasper.rotation_euler = Euler(rotation)

    def _add_liver(self, location: tuple[float, float, float] = (0,0,0), rotation: tuple[float, float, float] = (0,0,0), path:str|None = None):
        if path == None:
            path = os.path.join(os.getcwd(), "blender", "assets", "099283.fbx")

        bpy.ops.import_scene.fbx(filepath=path, global_scale=1)

        # liver = bpy.context.object
        # liver.location = Vector(location)
        # liver.rotation_euler = Euler(rotation)

    def _add_contraints(self):
        bpy.ops.object.select_camera(extend=False)
        cam = bpy.context.selected_objects[0]

        bpy.ops.object.select_pattern(pattern="Instrument*", extend=False)
        instruments = bpy.context.selected_objects
        
        for i in instruments:
            i.constraints.new(type='CHILD_OF')
            i.constraints["Child Of"].target = cam

        bpy.ops.object.select_pattern(pattern="Point*", extend=False)
        light = bpy.context.selected_objects[0]
        
        light.constraints.new(type='CHILD_OF')
        light.constraints["Child Of"].target = cam
    
    def _build_background_mesh(self, location: tuple[float, float, float] = (0,0,0), rotation: tuple[float, float, float] = (0,0,0)):
        bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=3, enter_editmode=True, align='WORLD', location=location, scale=(1, 1.5, .45))
        bpy.ops.mesh.flip_normals()
        bpy.ops.object.editmode_toggle()

        fat_background = bpy.context.object
        fat_background.name = "background"
        
        bpy.ops.object.shade_smooth_by_angle()

        # Add geo nodes mod
        mod = fat_background.modifiers.new("geo_nodes", type="NODES")

        if self.simple_shape:
            mod.node_group = load_geo_nodes_modifier("displace_mod", os.path.join(os.getcwd(), "src", "scene", "nodes", "simple_displace_mod.yml"))
        else:
            mod.node_group = load_geo_nodes_modifier("displace_mod", os.path.join(os.getcwd(), "src", "scene", "nodes", "displace_mod.yml"))

        mod['Socket_2'] = 2 # Resolution
        if self.simple_mat:
            mod['Socket_3'] = load_material("simple_fat_material", os.path.join(os.getcwd(), "src", "scene", "nodes", f"simple_fat_material.yml"))
        else:
            mod['Socket_3'] = load_material("pure_fat_material", os.path.join(os.getcwd(), "src", "scene", "nodes", f"pure_fat_material_{np.random.choice([2,3,4])}.yml")) # Fat Material 
        mod['Socket_4'] = load_material("organ_material", os.path.join(os.getcwd(), "src", "scene", "nodes", "organ_material.yml")) # Organ Material
        mod['Socket_5'] = 20.0 # Noise scale
        mod['Socket_6'] = np.random.uniform(0.5, 2.0) # Fat scale
        mod['Socket_7'] = self.seed # Seed
        mod['Socket_8'] = 0.8 # Organ density

    def _build_bowel(self, handles, bg, location: tuple[float, float, float] = (0,0,0), rotation: tuple[float, float, float] = (0,0,0)) -> None:
        bpy.ops.curve.primitive_bezier_curve_add(radius=1, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))

        curve = bpy.context.object
        curve.name = "Bowel"

        # Modifiy curves acording to handles
        bezier_points = curve.data.splines[0].bezier_points
        len_h = len(handles)
        len_b = len(bezier_points)
        if len_h > len_b:
            bezier_points.add(len_h - len_b)
        elif len_h < len_b:
            print("WARNING: Not enough handles given to create bowel curve")
        for h, p in zip(handles, bezier_points):
            p.co = h['co']
            p.handle_left = h['lh']
            p.handle_right = h['rh']

        mod = curve.modifiers.new("geo_nodes", type="NODES")

        if self.simple_shape:
           mod.node_group = load_geo_nodes_modifier("simple_bowel_mod", os.path.join(os.getcwd(), "src", "scene", "nodes", f"simple_bowel_mod.yml")) 
        else:
            mod.node_group = load_geo_nodes_modifier("bowel_mod", os.path.join(os.getcwd(), "src", "scene", "nodes", f"bowel_mod_{np.random.randint(1,4)}.yml"))
        
        mod['Socket_2'] = bg # Background
        mod['Socket_3'] = 2 # Resolution
        if self.simple_mat:
             mod['Socket_4'] = load_material("simple_bowel_mat", os.path.join(os.getcwd(), "src", "scene", "nodes", "simple_bowel_mat.yml")) # Material
        else:
            mod['Socket_4'] = load_material("bowel_mat", os.path.join(os.getcwd(), "src", "scene", "nodes", "bowel_mat.yml")) # Material
        mod['Socket_5'] = 15.0 # Fat amount
        mod['Socket_6'] = 0.03 # Fat scale
        mod['Socket_7'] = self.seed # Seed
        mod['Socket_8'] = np.random.uniform(0.01, 0.05) # Bowel offset

        curve.location = Vector(location)
        curve.rotation_euler = Euler(rotation)

    def _build(self) -> None:
        self._add_camera(location=(0.4, 0.19, 0.6), rotation=(math.radians(60),math.radians(0.0),math.radians(0.7)))
        self._add_light_source(location=(0.4, 0.19, 0.6))

        graspers = [
            (
                (0.3, 0.62, 0.38), 
                (math.radians(-80), math.radians(-36.9), math.radians(-256.9))
            ),
            (
                (0.3, 0.6, 0.37), 
                (math.radians(-87), math.radians(-27), math.radians(-185.8))
            ),
            (
                (0.49, 0.5, 0.3), 
                (math.radians(-87.1), math.radians(-25), math.radians(-167))
            ),
        ]
        np.random.shuffle(graspers)
        for i in range(np.random.randint(1, 2)):
            grasper_rand_fac_trans = 0.01
            grasper_rand_fac_rot = 5
            self._add_grasper(
                (
                    graspers[i][0][0]+np.random.uniform(-grasper_rand_fac_trans, grasper_rand_fac_trans),
                    graspers[i][0][1]+np.random.uniform(-grasper_rand_fac_trans, grasper_rand_fac_trans),
                    graspers[i][0][2]+np.random.uniform(-grasper_rand_fac_trans, grasper_rand_fac_trans),
                ),
                (
                    graspers[i][1][0]+math.radians(np.random.uniform(-grasper_rand_fac_rot, grasper_rand_fac_rot)),
                    graspers[i][1][1]+math.radians(np.random.uniform(-grasper_rand_fac_rot, grasper_rand_fac_rot)),
                    graspers[i][1][2]+math.radians(np.random.uniform(-grasper_rand_fac_rot, grasper_rand_fac_rot)),
                )
            )

        if np.random.choice(a=[False, True]):
            self._add_liver()

        self._build_background_mesh(location=(0.0,0.0,0.7), rotation=(0,0,0))

        def f_bowel_curve(x) -> float:
            return math.sin(x*2)*0.75
        handles = [
            dict(
                co=(-0.6, 0.0, -0.2),
                lh=(-0.61, 0.0, -0.2), 
                rh=(-0.59, 0.0, -0.2)
            )
        ]
        for i in range(4):
            rand_fac = 0.1
            handles.append(
                 dict( 
                    co=(i*(1.2/6)+np.random.uniform(-rand_fac,rand_fac), f_bowel_curve(i+np.random.uniform(-rand_fac,rand_fac)), 0),
                    lh=(((i-1)*(1.2/6)+np.random.uniform(-rand_fac,rand_fac))*0.5, f_bowel_curve((i-1)+np.random.uniform(-rand_fac,rand_fac))*0.5, 0),
                    rh=(((i+1)*(1.2/6)*0.5+np.random.uniform(-rand_fac,rand_fac))*0.5, f_bowel_curve((i+1)+np.random.uniform(-rand_fac,rand_fac))*0.5, 0),
                )
            )

        handles.append(
            dict(
                co=(0.6, 0,.0 -0.2), 
                lh=(0.61, 0,.0 -0.2), 
                rh=(0.59, 0,.0 -0.2)
            )
        )

        bpy.ops.object.select_pattern(extend=False, pattern="background")
        bg = bpy.context.object

        self._build_bowel(handles=handles, bg=bg, location=(0, 0.23926, 0,))

        self._add_contraints()

        if self.save_blend_file:
            bpy.ops.wm.save_as_mainfile(filepath=os.path.join(os.getcwd(), "blender", "blend_files", self.blend_file_name))

    def _render_image(self, trans: tuple[float, float, float], rotation: tuple[float, float, float], i, path, create_mask: bool):
        bpy.data.objects["Camera"].location = Vector(trans)
        bpy.data.objects["Camera"].rotation_euler = Euler(rotation)

        # redirect output to log file
        logfile = 'blender_render.log'
        open(logfile, 'a').close()
        old = os.dup(sys.stdout.fileno())
        sys.stdout.flush()
        os.close(sys.stdout.fileno())
        fd = os.open(logfile, os.O_WRONLY)

        bpy.context.scene.render.filepath = os.path.join(path, f"img_{i+1:05d}.png")
        bpy.ops.render.render(use_viewport=True, write_still=True)

        if create_mask:
            self._render_mask('Bowel', i, path)

        # disable output redirection
        os.close(fd)
        os.dup(old)
        os.close(old)

    def _render_mask(self, obj, i, path):
        bpy.ops.object.select_pattern(extend=False, pattern=obj)
        selected_obj = bpy.context.selected_objects[0]

        selected_obj.is_holdout = True

        bpy.context.scene.render.filepath = os.path.join(path, f"msk_{i+1:05d}.png")
        bpy.context.scene.render.image_settings.color_mode = 'RGBA'

        bpy.ops.render.render(use_viewport=True, write_still=True)

        bpy.context.scene.render.image_settings.color_mode = 'RGB'

        selected_obj.is_holdout = False

        # Post processing for mask
        image = cv2.imread(os.path.join(path, f"msk_{i+1:05d}.png"),  cv2.IMREAD_UNCHANGED)
        alpha_channel = image[:, :, 3]
        _, binary_mask = cv2.threshold(alpha_channel, 0, 255, cv2.THRESH_BINARY)
        binary_mask = cv2.bitwise_not(binary_mask)
        cv2.imwrite(os.path.join(path, f"msk_{i+1:05d}.png"), binary_mask)

    def _render_video(self, trans: tuple[float, float, float], rotation: tuple[float, float, float], i, path, create_masks: bool):
        base_path = os.path.join(path, f"vid_{i+1:05d}")
        os.makedirs(base_path)

        for i in range(100):
            bpy.context.scene.frame_current = i
            self._render_image(trans, rotation, i, base_path, create_masks)

    def render(
            self, 
            amount: int, 
            res: tuple[int, int],
            path,
            create_masks: bool = False,
            progress_bar:tqdm|None=None,
            video:bool = False
        ) -> None:
        os.makedirs(path, exist_ok=True)

        render = bpy.context.scene.render

        render.resolution_x = res[0]
        render.resolution_y = res[1]

        # Different camera angles of the scene
        cam_pos = [
            (
                (0.4, 0.19, 0.6),
                (math.radians(60),math.radians(0.0),math.radians(0.7))
            ),
        ]

        render.use_compositing = False

        # Set the device_type
        using_gpu = False
        cycles_preferences = bpy.context.preferences.addons['cycles'].preferences
        cycles_preferences.refresh_devices()

        if "CUDA" in [d.type for d in cycles_preferences.devices]:
            using_gpu = True
            cycles_preferences.compute_device_type = "CUDA"

            # Set the device and feature set
            bpy.context.scene.cycles.device = "GPU"
        
        last_idx = max((int(s[4:9]) for s in os.listdir(path)), default=-1)
        print(f"Rendering images: " + ("(CUDA)" if using_gpu else "(CPU)"))

        pb_avail = progress_bar is not None

        for i in tqdm(range(last_idx, last_idx+amount, 1), disable=pb_avail):
            trans, rot = cam_pos[0]
            trans = (
                trans[0] + np.random.uniform(-0.38, 0),
                trans[1] + np.random.uniform(-1, 0),
                trans[2]
            )
            rot = (
                rot[0] + math.radians(np.random.uniform(0, 0.05)),
                rot[1],
                rot[2]
            )
            # trans=tuple(x+np.random.uniform(-0.1, 0.1) for x in trans)
            # rot=tuple(x+np.random.uniform(-0.075, 0.075) for x in rot)

            if video:
                self._render_video(trans, rot, i, path, create_masks)
            else:
                self._render_image(trans, rot, i, path, create_masks)

            if progress_bar is not None:
                progress_bar.update(1)