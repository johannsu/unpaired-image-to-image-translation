import argparse
import os
from src.scene.bowel import BowelScene

def main(
    seed: int, 
    blend_file_name: str,
    num_renders: int|None,
    render_res: list,
    render_path: str,
    render_mask: bool,
    render_video:bool,
    simle_mat:bool,
    simple_shape:bool
):
    scene = BowelScene(seed=seed, blend_file_name=blend_file_name, save_blend_file=True, simple_mat=simle_mat, simple_shape=simple_shape)
    if num_renders != None:
        os.makedirs(render_path, exist_ok=True)
        # if not os.path.exists(render_path):
        #     raise ValueError("The render path specified does not exist")
        if len(render_res) != 2 or render_res[0] <= 0 or render_res[1] <= 0:
            raise ValueError("Resolution must be in the format '--render_res 1920 1080' with two ints > 0")
        if num_renders < 1:
            raise ValueError("--num_renders must be >= 1")
    
        scene.render(num_renders, (render_res[0], render_res[1]), render_path, render_mask, None, render_video)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', type=int,
                    help='Seed used to generate scene')
    parser.add_argument('--blend_file_name', type=str,
                    default="out.blend",
                    help='file name of the blend file')
    parser.add_argument('--num_renders', nargs='?', type=int,
                    help='number of images to render')
    parser.add_argument('--render_res', nargs='+', type=int,
                    default=[1920,1080],
                    help='the resolution of the rendered images')
    parser.add_argument('--render_path', type=str,
                    default="renders",
                    help='directory of the rendered images')
    parser.add_argument('--render_mask', type=bool,
                    default=True,
                    help='Render a mask of the bowel')
    parser.add_argument('--render_video', action=argparse.BooleanOptionalAction, type=bool,
                    default=False,
                    help='Render a video of the scene')

    args = parser.parse_args()
    main(args.seed, args.blend_file_name, args.num_renders, args.render_res, args.render_path, args.render_mask, args.render_video)