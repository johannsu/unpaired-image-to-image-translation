from lightning.pytorch.cli import LightningCLI
import os
import torch
import fsspec
from dotenv import load_dotenv
import src.nn.cycle_gan.cyclegan
import src.nn.munit.munit
import src.nn.cut.cut
import src.nn.segmentation.segmentation
import src.nn.datasets.gan_dataset
import src.nn.datasets.segmentation_dataset

load_dotenv(override=True)
torch.set_float32_matmul_precision('medium')

class MyCli(LightningCLI):
    def add_arguments_to_parser(self, parser):
        parser.add_argument("name")
        # parser.add_argument("version")
        parser.link_arguments("name", "trainer.logger.init_args.name")
        # parser.link_arguments(("name", "version"), "trainer.callbacks.init_args.dirpath", compute_fn=lambda x,y: f"s3://bachelor-thesis/checkpoints/{x}/{y}")
        # parser.link_arguments(("name", "version"), "trainer.default_root_dir", compute_fn=lambda x,y: f"s3://bachelor-thesis/checkpoints/{x}/{y}")
        parser.link_arguments("name", "trainer.callbacks.init_args.dirpath", compute_fn=self.compute_versioned_path)
        parser.link_arguments("name", "trainer.default_root_dir", compute_fn=self.compute_versioned_path)
        # parser.link_arguments("version", "trainer.logger.init_args.version")
        parser.link_arguments("data.init_args.batch_size", "model.init_args.batch_size")
        parser.link_arguments("data.init_args.predict_to_path", "model.init_args.predict_save_path")

    def compute_versioned_path(self, name):
        base_path = f"checkpoints/{name}"
        os.makedirs(base_path, exist_ok=True)
        existing_versions = [int(d.split('_')[1]) for d in os.listdir(base_path) if d.startswith('version_')]
        next_version = max(existing_versions, default=-1) + 1
        return f"{base_path}/version_{next_version}"


def cli_main():
    MyCli(parser_kwargs={"parser_mode": "omegaconf"})

if __name__ == '__main__':
    cli_main()